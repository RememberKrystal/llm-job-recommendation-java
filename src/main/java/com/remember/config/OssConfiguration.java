package com.remember.config;

import com.remember.properties.AliOssProperties;
import com.remember.utils.AliOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类，用于创建AliOssUtil对象
 */

@Configuration // 用于标志为一个配置类
@Slf4j // 日志
public class OssConfiguration {
    @Bean
    public AliOssUtil aliOssUtil(AliOssProperties aliOSSProperties) {
        log.info("开始创建阿里云文件上传工具类对象：{}", aliOSSProperties);
        return new AliOssUtil(aliOSSProperties.getEndpoint(),
                aliOSSProperties.getAccessKeyId(),
                aliOSSProperties.getAccessKeySecret(),
                aliOSSProperties.getBucketName());
    }
}
