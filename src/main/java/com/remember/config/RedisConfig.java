package com.remember.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/7/28 15:09
 * @Description : Redis配置类
 */
@Configuration
public class RedisConfig {

    /**
     * 创建RedisTemplate bean，用于操作Redis数据库。
     * 该方法配置了Redis模板的序列化方式，包括键的序列化和值的序列化。
     *
     * @param redisConnectionFactory Redis连接工厂，用于创建Redis连接。
     * @return RedisTemplate实例，配置了字符串序列化器用于键和值的序列化。
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        // 设置ConnectionFactory，用于建立与Redis服务器的连接。
        template.setConnectionFactory(redisConnectionFactory);
        // 设置键的序列化方式为StringRedisSerializer，确保键的序列化一致性。
        template.setKeySerializer(new StringRedisSerializer());
        // 设置值的序列化方式为StringRedisSerializer，可以根据实际需求调整。
        template.setValueSerializer(new StringRedisSerializer());
        return template;
    }

}
