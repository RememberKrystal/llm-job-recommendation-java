package com.remember.config;

import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.domain.po.PostSchoolTbl;
import com.remember.domain.po.PostTbl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/9/21 15:24
 * @Description : 统计非校园岗位/校园的单个每日访问量
 */
@Configuration
@EnableScheduling // 开启定时任务
public class PostCacheConfig {

    @Bean(name = "ipCache")
    public ConcurrentHashMap<String, Integer> ipCache() {
        return new ConcurrentHashMap<>();
    }

    @Bean(name = "schoolIpCache")
    public ConcurrentHashMap<String, Integer> schoolIpCache() {
        return new ConcurrentHashMap<>();
    }

    // 定时任务，每天清空一次缓存
    @Scheduled(cron = "0 0 0 * * ?")
    public void clearCache() {
        try {
            Map<Integer, Integer> valueCount = new HashMap<>();

            PostCacheConfig postCacheConfig = new PostCacheConfig();
            // 1.更新post_tbl表中的浏览量
            for (String key : postCacheConfig.ipCache().keySet()) {
                Integer value = postCacheConfig.ipCache().get(key);
                valueCount.put(value, valueCount.getOrDefault(value, 0) + 1);
            }
            // 遍历valueCount，更新数据库浏览量
            for (Integer key : valueCount.keySet()) {
                int count = valueCount.get(key);
                PostTbl postTbl = Db.lambdaQuery(PostTbl.class).eq(PostTbl::getId, key).one();
                postTbl.setViews(postTbl.getViews() + count);
                Db.updateById(postTbl);
            }
            valueCount.clear();// 清空

            // 2.更新postSchool_tbl表的浏览量
            for (String key : postCacheConfig.schoolIpCache().keySet()) {
                Integer value = postCacheConfig.schoolIpCache().get(key);
                valueCount.put(value, valueCount.getOrDefault(value, 0) + 1);
            }
            // 遍历valueCount，更新数据库浏览量
            for (Integer key : valueCount.keySet()) {
                int count = valueCount.get(key);
                PostSchoolTbl postSchoolTbl = Db.lambdaQuery(PostSchoolTbl.class).eq(PostSchoolTbl::getId, key).one();
                postSchoolTbl.setViews(postSchoolTbl.getViews() + count);
                Db.updateById(postSchoolTbl);
            }
            valueCount.clear();// 清空

            ipCache().clear();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
