package com.remember.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "aliyun.sms")
@Data
public class AliSmsProperties {
    // 域名
    private String endpoint;

    // 秘钥
    private String accessKeyId;

    // 秘钥
    private String accessKeySecret;

    // 签名名称
    private String signName;

    // 模板名称
    private String templateCode;

}
