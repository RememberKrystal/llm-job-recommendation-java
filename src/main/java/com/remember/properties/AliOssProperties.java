package com.remember.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "aliyun.oss")
@Data
public class AliOssProperties {
    // 域名
    private String endpoint;

    // 秘钥
    private String accessKeyId;

    // 秘钥
    private String accessKeySecret;

    // 存储空间
    private String bucketName;

}
