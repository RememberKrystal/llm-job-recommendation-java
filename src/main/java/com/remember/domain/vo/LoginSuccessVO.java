package com.remember.domain.vo;

import com.remember.domain.po.UserTbl;
import lombok.Data;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/7/28 23:15
 * @Description :
 */
@Data
public class LoginSuccessVO {

    private String token;

    private UserTbl user;

}
