package com.remember.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/9/23 14:33
 * @Description :  推文评论VO
 */
@Data
public class EssayCommentsVO {

    // 是否是本人评论
    private boolean isSelf;

    // 评论用户名
    private String  username;

    // 评论内容
    private String comments;

    // 评论图片集合
    private List<String> images;

    // 评论时间
    private LocalDateTime whenCreated;

    // 用户id
    private Integer userId;

    // 推文id
    private Integer essayId;
}
