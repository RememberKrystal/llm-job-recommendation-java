package com.remember.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/9/22 12:45
 * @Description : 发布推文DTO
 */
@Data
public class PublishEssayDTO {
    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "推文内容")
    private String description;

    @ApiModelProperty(value = "状态（1公开，2 私密）")
    private Integer state;

    @ApiModelProperty(value = "图片集合")
    private List<String> imageList;
}
