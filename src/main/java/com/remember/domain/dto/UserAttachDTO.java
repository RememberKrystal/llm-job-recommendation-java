package com.remember.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/8/22 10:59
 * @Description : 用户附加信息
 */
@Data
public class UserAttachDTO {
    @ApiModelProperty(value = "爱好")
    private String hobby;

    @ApiModelProperty(value = "专业id")
    private Integer specializedId;

    @ApiModelProperty(value = "学历(0 高中 1专科 2本科 3研究生 4 博士）")
    private Integer degree;

    @ApiModelProperty(value = "平均分数（百分制）")
    private String score;

    @ApiModelProperty(value = "是否实习(0 是 1 否)")
    private Integer isExercitation;

    @ApiModelProperty(value = "实习公司名称")
    private String firmName;

    @ApiModelProperty(value = "实习公司父类型id")
    private Integer firmTypeId;

    @ApiModelProperty(value = "实习公司子类型id")
    private Integer firmTypeSonId;

    @ApiModelProperty(value = "实习公司的最低薪资")
    private BigDecimal moneyMin;

    @ApiModelProperty(value = "实习公司的最高薪资")
    private BigDecimal moneyMax;

    @ApiModelProperty(value = "期望公司名称")
    private String expectationFirm;

    @ApiModelProperty(value = "期望公司的父类型id")
    private Integer expectationTypeId;

    @ApiModelProperty(value = "期望公司的子类型id")
    private Integer expectationTypeSonId;

    @ApiModelProperty(value = "期望公司的最低薪资")
    private BigDecimal expectationMoneyMin;

    @ApiModelProperty(value = "期望公司的最高薪资")
    private BigDecimal expectationMoneyMax;
}
