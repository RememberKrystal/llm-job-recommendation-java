package com.remember.domain.dto;

import lombok.Data;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/8/30 11:07
 * @Description :
 */
@Data
public class GetPostDTO {

    private Integer page; // 当前页码

    private Integer size; // 每页显示数量

    private String name;
}
