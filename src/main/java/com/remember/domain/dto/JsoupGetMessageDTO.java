package com.remember.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/8/21 10:33
 * @Description :
 */
@Data
public class JsoupGetMessageDTO {

    @ApiModelProperty(value = "关键词")
    private String keyword;

    @ApiModelProperty(value = "页码")
    private String page;
}
