package com.remember.domain.dto;

import lombok.Data;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/9/21 18:00
 * @Description :
 */
@Data
public class CollectionPostDTO {

    private int postId;

    private int school; // 1 校招 2 非校招
}
