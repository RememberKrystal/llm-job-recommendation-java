package com.remember.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/7/28 15:18
 * @Description : 用户注册
 */
@ApiModel(value="用户注册", description="")
@Data
public class UserRegisterDTO {
    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "电话号码")
    private String phone;

    @ApiModelProperty(value = "验证码")
    private String code;

}
