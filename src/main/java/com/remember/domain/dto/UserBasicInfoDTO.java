package com.remember.domain.dto;

import lombok.Data;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/8/22 10:54
 * @Description : 用户基础信息
 */
@Data
public class UserBasicInfoDTO {

    private String name;

    private Integer gender;

    private Integer age;

    private String avatar; // 头像
}
