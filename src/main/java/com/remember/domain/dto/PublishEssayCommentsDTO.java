package com.remember.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/9/23 14:38
 * @Description : 发布推文评论
 */
@Data
public class PublishEssayCommentsDTO {

    // 推文id
    private Integer essayId;

    // 评论内容
    private String comments;

    // 图片集合
    private List<String> images;

}
