package com.remember.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("specialized_tbl")
@ApiModel(value="SpecializedTbl对象", description="")
public class SpecializedTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "专业id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "专业名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "所属行业id")
    @TableField("industry_id")
    private Integer industryId;

    @ApiModelProperty(value = "所属行业的子id")
    @TableField("industry_son_id")
    private Integer industrySonId;


}
