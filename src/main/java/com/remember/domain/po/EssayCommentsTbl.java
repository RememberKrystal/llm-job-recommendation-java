package com.remember.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 推文一级评论表
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("essay_comments_tbl")
@ApiModel(value="EssayCommentsTbl对象", description="推文一级评论表")
public class EssayCommentsTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "推文id")
    @TableField("essay_id")
    private Integer essayId;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "评论内容")
    @TableField("comments")
    private String comments;

    @ApiModelProperty(value = "图片JSON")
    @TableField("images")
    private String images;

    @ApiModelProperty(value = "评论时间")
    @TableField("when_created")
    private LocalDateTime whenCreated;


}
