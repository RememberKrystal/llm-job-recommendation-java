package com.remember.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户收藏岗位信息表
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_post_collection_tbl")
@ApiModel(value="UserPostCollectionTbl对象", description="用户收藏岗位信息表")
public class UserPostCollectionTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "岗位id")
    @TableField("post_id")
    private Integer postId;

    @ApiModelProperty(value = "（1是校招岗位，2不是校招岗位）")
    @TableField("school")
    private Integer school;

    @ApiModelProperty(value = "创建时间")
    @TableField("when_created")
    private LocalDateTime whenCreated;

    public UserPostCollectionTbl(Integer userId, Integer postId, Integer school, LocalDateTime whenCreated) {
        this.userId = userId;
        this.postId = postId;
        this.school = school;
        this.whenCreated = whenCreated;
    }

    public UserPostCollectionTbl(Integer id, Integer userId, Integer postId, Integer school, LocalDateTime whenCreated) {
        this.id = id;
        this.userId = userId;
        this.postId = postId;
        this.school = school;
        this.whenCreated = whenCreated;
    }

    public UserPostCollectionTbl() {
    }
}
