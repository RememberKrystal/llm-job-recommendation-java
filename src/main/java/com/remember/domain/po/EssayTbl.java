package com.remember.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 推文表

 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("essay_tbl")
@ApiModel(value="EssayTbl对象", description="推文表")
public class EssayTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "推文内容")
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "状态（1公开，2 私密）")
    @TableField("state")
    private Integer state;

    @ApiModelProperty(value = "图片JSON")
    @TableField("image")
    private String image;

    @ApiModelProperty(value = "创建时间")
    @TableField("when_created")
    private LocalDateTime whenCreated;

    @ApiModelProperty(value = "点赞数量")
    @TableField("support_number")
    private Integer supportNumber;

    @ApiModelProperty(value = "浏览量")
    @TableField("views")
    private Integer views;

    @ApiModelProperty(value = "收藏数量")
    @TableField("collection_number")
    private Integer collectionNumber;

    @TableField(exist = false)
    private boolean collection; // 是否收藏

    @TableField(exist = false)
    private boolean support; // 是否点赞

    @TableField(exist = false)
    private List<String> imageList; // 图片集合

}
