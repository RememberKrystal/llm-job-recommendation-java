package com.remember.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 搜索历史表
 * </p>
 *
 * @author remember
 * @since 2024-11-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("search_history_tbl")
public class SearchHistoryTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 历史
     */
    @TableField("history")
    private String history;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 最近一次搜索时间
     */
    @TableField("when_created")
    private LocalDateTime whenCreated;


}
