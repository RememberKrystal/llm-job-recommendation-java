package com.remember.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("post_tbl")
@ApiModel(value="PostTbl对象", description="")
public class PostTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "岗位名称")
    @TableField("post_name")
    private String postName;

    @ApiModelProperty(value = "最低薪资")
    @TableField("money_min")
    private String moneyMin;

    @ApiModelProperty(value = "最高薪资")
    @TableField("money_max")
    private String moneyMax;

    @ApiModelProperty(value = "头像")
    @TableField("image")
    private String image;

    @ApiModelProperty(value = "城市")
    @TableField("city")
    private String city;

    @ApiModelProperty(value = "工作地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "工作经验")
    @TableField("experience")
    private String experience;

    @ApiModelProperty(value = "学历")
    @TableField("degree")
    private String degree;

    @ApiModelProperty(value = "工作描述")
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "公司名称")
    @TableField("firm_name")
    private String firmName;

    @ApiModelProperty(value = "状态（1上架2下架3待审核）")
    @TableField("state")
    private Integer state;

    @ApiModelProperty(value = "行业id")
    @TableField("industry_id")
    private Integer industryId;

    @ApiModelProperty(value = "子行业id")
    @TableField("industry_son_id")
    private Integer industrySonId;

    @ApiModelProperty(value = "浏览量")
    @TableField("views")
    private Integer views;

    @ApiModelProperty(value = "收藏数量")
    @TableField("collection_number")
    private Integer collectionNumber;

    @ApiModelProperty(value = "是否收藏")
    @TableField(exist = false) // 不映射数据库字段
    private boolean collection; // 用户是否收藏

    public PostTbl(String postName, String moneyMin, String moneyMax, String image, String city, String address, String experience, String degree, String description, String firmName, Integer state, Integer industryId, Integer industrySonId,Integer views,Integer collectionNumber) {
        this.postName = postName;
        this.moneyMin = moneyMin;
        this.moneyMax = moneyMax;
        this.image = image;
        this.city = city;
        this.address = address;
        this.experience = experience;
        this.degree = degree;
        this.description = description;
        this.firmName = firmName;
        this.state = state;
        this.industryId = industryId;
        this.industrySonId = industrySonId;
        this.views = views;
        this.collectionNumber = collectionNumber;
    }

    public PostTbl() {
    }
}
