package com.remember.domain.po;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_info_tbl")
@ApiModel(value="UserInfoTbl对象", description="")
public class UserInfoTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "爱好")
    @TableField("hobby")
    private String hobby;

    @ApiModelProperty(value = "专业id")
    @TableField("specialized_id")
    private Integer specializedId;

    // 专业
    @TableField(exist = false)
    private String specialized;

    @ApiModelProperty(value = "学历(0 高中 1专科 2本科 3研究生 4 博士）")
    @TableField("degree")
    private Integer degree;

    @ApiModelProperty(value = "平均分数（百分制）")
    @TableField("score")
    private String score;

    @ApiModelProperty(value = "是否实习(0 是 1 否)")
    @TableField("is_exercitation")
    private Integer isExercitation;

    @ApiModelProperty(value = "实习公司名称")
    @TableField("firm_name")
    private String firmName;

    @ApiModelProperty(value = "实习公司父类型id")
    @TableField("firm_type_id")
    private Integer firmTypeId;

    // 实习公司父类型
    @TableField(exist = false)
    private String firmType;

    @ApiModelProperty(value = "实习公司子类型id")
    @TableField("firm_type_son_id")
    private Integer firmTypeSonId;

    // 实习公司子类型
    @TableField(exist = false)
    private String firmTypeSon;

    @ApiModelProperty(value = "实习公司的最低薪资")
    @TableField("money_min")
    private BigDecimal moneyMin;

    @ApiModelProperty(value = "实习公司的最高薪资")
    @TableField("money_max")
    private BigDecimal moneyMax;

    @ApiModelProperty(value = "期望公司名称")
    @TableField("expectation_firm")
    private String expectationFirm;

    @ApiModelProperty(value = "期望公司的父类型id")
    @TableField("expectation_type_id")
    private Integer expectationTypeId;

    // 期望公司父类型
    @TableField(exist = false)
    private String expectationType;

    @ApiModelProperty(value = "期望公司的子类型id")
    @TableField("expectation_type_son_id")
    private Integer expectationTypeSonId;

    // 期望公司子类型
    @TableField(exist = false)
    private String expectationTypeSon;

    @ApiModelProperty(value = "期望公司的最低薪资")
    @TableField("expectation_money_min")
    private BigDecimal expectationMoneyMin;

    @ApiModelProperty(value = "期望公司的最高薪资")
    @TableField("expectation_money_max")
    private BigDecimal expectationMoneyMax;


}
