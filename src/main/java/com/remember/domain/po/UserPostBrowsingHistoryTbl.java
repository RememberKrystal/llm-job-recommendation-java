package com.remember.domain.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户岗位浏览记录
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_post_browsing_history_tbl")
@ApiModel(value="UserPostBrowsingHistoryTbl对象", description="用户岗位浏览记录")
public class UserPostBrowsingHistoryTbl implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "岗位id")
    @TableField("post_id")
    private Integer postId;

    @ApiModelProperty(value = "（1是校招岗位，2不是校招岗位）")
    @TableField("school")
    private Integer school;

    @ApiModelProperty(value = "创建时间")
    @TableField("when_created")
    private LocalDateTime whenCreated;


}
