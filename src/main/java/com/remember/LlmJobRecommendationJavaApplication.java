package com.remember;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@MapperScan("com.remember.mapper")
@EnableAsync // 开启异步，告诉SpringBoot带有@Async注解的方法异步执行
public class LlmJobRecommendationJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(LlmJobRecommendationJavaApplication.class, args);
    }

}
