package com.remember.controller;


import cn.hutool.json.JSONUtil;
import com.remember.domain.po.IndustryInfoTbl;
import com.remember.result.Result;
import com.remember.service.IIndustryInfoTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@RestController
@RequestMapping("/user/industry-info-tbl")
@Api(tags = "子行业接口")
@RequiredArgsConstructor
public class IndustryInfoTblController {

    private final IIndustryInfoTblService industryInfoTblService;

    private final StringRedisTemplate stringRedisTemplate;

    private static final String KEY = "cache:industryInfo:";

    /**
     * 根据父行业id查询相关子行业
     */
    @GetMapping("/getById")
    @ApiOperation(value = "根据父行业id查询相关子行业")
    public Result<List<IndustryInfoTbl>> getIndustryInfoById(@ApiParam(value = "父行业id") Integer id) {
        // 查询缓存
        String industryInfo = stringRedisTemplate.opsForValue().get(KEY + id);
        if (industryInfo != null){
            // 有数据，直接返回
            List<IndustryInfoTbl> list = JSONUtil.toList(industryInfo, IndustryInfoTbl.class);
            return Result.success(list);
        }
        // 不存在,查询数据库
        List<IndustryInfoTbl> list = industryInfoTblService.lambdaQuery()
                .eq(IndustryInfoTbl::getIndustryId, id)
                .list();
        // 写入缓存
        stringRedisTemplate.opsForValue().set(KEY + id, JSONUtil.toJsonStr(list));
        // 返回数据
        return Result.success(list);
    }
}
