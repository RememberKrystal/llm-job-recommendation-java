package com.remember.controller;


import com.remember.context.BaseContext;
import com.remember.domain.dto.PublishEssayCommentsDTO;
import com.remember.domain.po.EssayCommentsTbl;
import com.remember.domain.vo.EssayCommentsVO;
import com.remember.result.Result;
import com.remember.service.IEssayCommentsTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 推文一级评论表 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/essay-comments-tbl")
@Api(tags = "推文一级评论表相关接口")
@RequiredArgsConstructor
public class EssayCommentsTblController {

    private final IEssayCommentsTblService essayCommentsTblService;

    @GetMapping
    @ApiOperation("根据推文id查询一级评论")
    public Result<List<EssayCommentsVO>> getEssayComments(Integer essayId) {
        return essayCommentsTblService.getEssayComments(essayId);
    }

    @PostMapping
    @ApiOperation("发布一级评论")
    public Result<?> publishEssayComments(@RequestBody PublishEssayCommentsDTO publishEssayCommentsDTO) {
        return essayCommentsTblService.publishEssayComments(publishEssayCommentsDTO);
    }

    @DeleteMapping
    @ApiOperation("删除一级评论")
    public Result<?> deleteEssayComments(Integer id) {
        // 查看评论是否存在，且是自己的
        EssayCommentsTbl essayCommentsTbl = essayCommentsTblService.getById(id);
        if (essayCommentsTbl == null || essayCommentsTbl.getUserId() != BaseContext.getCurrentId().intValue()){
            return Result.error("评论不存在或无权限删除");
        }
        // 执行删除
        essayCommentsTblService.removeById(id);
        return Result.success();
    }

}
