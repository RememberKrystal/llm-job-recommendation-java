package com.remember.controller;


import cn.hutool.json.JSONUtil;
import com.remember.domain.po.SpecializedTbl;
import com.remember.result.Result;
import com.remember.service.ISpecializedTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@RestController
@RequestMapping("/user/specialized-tbl")
@RequiredArgsConstructor
@Api(tags = "专业相关接口")
public class SpecializedTblController {

    private final ISpecializedTblService specializedTblService;

    private final StringRedisTemplate stringRedisTemplate;

    private final static String KEY = "cache:specialized:";

    /**
     * 展示全部专业
     */
    @GetMapping("/getAll")
    @ApiOperation(value = "展示全部专业")
    public Result<List<SpecializedTbl>> getAll() {
        // 查询Redis是否有数据
        String result = stringRedisTemplate.opsForValue().get(KEY);
        if (result != null){
            // 有数据直接返回
            List<SpecializedTbl> list = JSONUtil.toList(result, SpecializedTbl.class);
            return Result.success(list);
        }
        //没有数据，查询数据库
        List<SpecializedTbl> list = specializedTblService.list();
        // 存入Redis
        stringRedisTemplate.opsForValue().set(KEY, JSONUtil.toJsonStr(list));
        // 返回结果
        return Result.success(list);
    }
}
