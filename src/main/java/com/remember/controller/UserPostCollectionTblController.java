package com.remember.controller;


import com.remember.domain.dto.CollectionPostDTO;
import com.remember.domain.po.PostSchoolTbl;
import com.remember.domain.po.PostTbl;
import com.remember.result.Result;
import com.remember.service.IUserPostCollectionTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.management.relation.RelationSupport;
import java.util.List;

/**
 * <p>
 * 用户收藏岗位信息表 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/user-post-collection-tbl")
@RequiredArgsConstructor
@Api(tags = "用户收藏岗位信息表相关接口")
public class UserPostCollectionTblController {

    private final IUserPostCollectionTblService userPostCollectionTblService;

    @GetMapping
    @ApiOperation("查询当前用户收藏的普通岗位信息")
    public Result<List<PostTbl>> getUserCollectionPost(){
        return userPostCollectionTblService.getUserCollectionPost();
    }

    @GetMapping("/school")
    @ApiOperation("查询当前用户收藏的校园岗位信息")
    public Result<List<PostSchoolTbl>> getUserCollectionSchoolPost(){
        return userPostCollectionTblService.getUserCollectionSchoolPost();
    }

    @PostMapping
    @ApiOperation("收藏/取消收藏岗位")
    public Result<?> collectionPost(@RequestBody CollectionPostDTO collectionPostDTO){
        return userPostCollectionTblService.collectionPost(collectionPostDTO);
    }

}
