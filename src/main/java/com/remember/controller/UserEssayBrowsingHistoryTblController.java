package com.remember.controller;


import com.remember.context.BaseContext;
import com.remember.domain.po.EssayTbl;
import com.remember.result.Result;
import com.remember.service.IUserEssayBrowsingHistoryTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户推文浏览记录表 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/user-essay-browsing-history-tbl")
@Api(tags = "用户推文浏览记录表相关接口")
@RequiredArgsConstructor
public class UserEssayBrowsingHistoryTblController {

    private final IUserEssayBrowsingHistoryTblService userEssayBrowsingHistoryTblService;

    @GetMapping
    @ApiOperation("查询当前用户的推文浏览记录")
    public Result<List<EssayTbl>> getUserEssayBrowsingHistory(){
        return userEssayBrowsingHistoryTblService.getUserEssayBrowsingHistory();
    }

    @DeleteMapping
    @ApiOperation("清空当前用户的推文浏览记录")
    public Result<?> deleteUserEssayBrowsingHistory(){
        return userEssayBrowsingHistoryTblService.deleteUserEssayBrowsingHistory();
    }
}
