package com.remember.controller;


import com.remember.domain.po.EssayTbl;
import com.remember.result.Result;
import com.remember.service.IUserEssayCollectionTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户推文收藏表 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/user-essay-collection-tbl")
@RequiredArgsConstructor
@Api(tags = "用户推文收藏表相关接口")
public class UserEssayCollectionTblController {

    private final IUserEssayCollectionTblService userEssayCollectionTblService;

    @GetMapping
    @ApiOperation("查询当前用户的推文收藏记录")
    public Result<List<EssayTbl>> getUserEssayCollection() {
        return userEssayCollectionTblService.getUserEssayCollection();
    }

    @PostMapping
    @ApiOperation("收藏/取消收藏推文")
    public Result<?> collectionEssay(Integer id) {
        return userEssayCollectionTblService.collectionEssay(id);
    }


}
