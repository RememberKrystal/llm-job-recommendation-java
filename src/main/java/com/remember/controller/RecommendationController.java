package com.remember.controller;

import com.remember.context.BaseContext;
import com.remember.domain.po.PostTbl;
import com.remember.result.Result;
import com.remember.service.RecommendationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/12/3 10:04
 * @Description : 混合推荐算法实现（猜你喜欢）
 */
@RestController
@RequestMapping("/user/api/recommendations")
@Api(tags = "猜你喜欢-混合推荐算法（协同过滤、基于内容推荐）")
public class RecommendationController {

    @Autowired
    private RecommendationService recommendationService;

    @GetMapping()
    @ApiOperation(value = "猜你喜欢")
    public Result<List<PostTbl>> getRecommendations(
            @RequestParam Integer page,
            @RequestParam Integer size) {
        Long userId = BaseContext.getCurrentId(); // 获取当前登录用户的id
        List<PostTbl> recommendations = recommendationService.recommendPosts(userId, page, size);
        return Result.success(recommendations);
    }
}


