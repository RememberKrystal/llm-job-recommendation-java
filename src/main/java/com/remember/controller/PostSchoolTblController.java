package com.remember.controller;


import com.remember.domain.po.PostSchoolTbl;
import com.remember.result.Result;
import com.remember.service.IPostSchoolTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 秋招春招信息表
 * 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/post-school-tbl")
@Api(tags = "校园岗位信息相关接口")
@RequiredArgsConstructor
public class PostSchoolTblController {

    private final IPostSchoolTblService postSchoolTblService;

    @GetMapping
    @ApiOperation("查询全部的校园招聘岗位信息")
    public Result<List<PostSchoolTbl>> getAllPostSchool(int page, int size) {
        return postSchoolTblService.getAllPostSchool(page, size);
    }

    @GetMapping("/getById")
    @ApiOperation("根据岗位id查询校园招聘岗位信息")
    public Result<PostSchoolTbl> getPostSchoolGetById(Integer id, HttpServletRequest request) {
        return postSchoolTblService.getPostSchoolGetById(id, request);
    }

    @GetMapping("/getByYearOrSchool")
    @ApiOperation("根据年份/春、秋招查询校园招聘岗位信息")
    public Result<List<PostSchoolTbl>> getByYearOrSchool(@ApiParam("年份") @RequestParam(required = false) Integer year,
                                                         @ApiParam("春、秋招") @RequestParam(required = false) Integer school,
                                                         int page,
                                                         int size) {
        return postSchoolTblService.getByYearOrSchool(year, school, page, size);
    }

}
