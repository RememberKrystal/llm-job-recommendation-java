package com.remember.controller;


import com.remember.domain.dto.UserAttachDTO;
import com.remember.domain.po.UserInfoTbl;
import com.remember.result.Result;
import com.remember.service.IUserInfoTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@RestController
@RequestMapping("/user/user-info-tbl")
@RequiredArgsConstructor
@Api(tags = "附加信息接口")
public class UserInfoTblController {

    private final IUserInfoTblService userInfoTblService;


    /**
     * 附加信息填写
     */
    @PostMapping("/attachInfo")
    @ApiOperation(value = "附加信息填写")
    public Result<String> attachInfo(@RequestBody UserAttachDTO userAttachDTO) {
        return userInfoTblService.attachInfo(userAttachDTO);
    }

    /**
     * 获取当前用户的附加信息
     */
    @GetMapping("/getInfo")
    @ApiOperation(value = "获取当前用户的附加信息")
    public Result<UserInfoTbl> getInfo() {
        return userInfoTblService.getInfo();
    }

    /**
     * 修改用户的附加信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改用户的附加信息")
    public Result<?> updateUser(@RequestBody UserAttachDTO userAttachDTO) {
        return userInfoTblService.updateAttachInfo(userAttachDTO);
    }


}
