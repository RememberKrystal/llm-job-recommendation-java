package com.remember.controller;


import com.remember.domain.po.PostTbl;
import com.remember.result.Result;
import com.remember.service.IUserPostBrowsingHistoryTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户岗位浏览记录 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/user-post-browsing-history-tbl")
@Api(tags = "用户岗位浏览记录相关接口")
@RequiredArgsConstructor
public class UserPostBrowsingHistoryTblController {

    private final IUserPostBrowsingHistoryTblService userPostBrowsingHistoryTblService;

    @GetMapping
    @ApiOperation("查询当前用户的岗位浏览记录")
    public Result<List<?>> getUserPostBrowsingHistory(Integer school, int page, int size) {
        return Result.success(userPostBrowsingHistoryTblService.getUserPostBrowsingHistory(school, page, size));
    }

    @DeleteMapping
    @ApiOperation("清空当前用户的岗位浏览记录（普通岗位/校招岗位）")
    public Result<?> deleteUserPostBrowsingHistory(Integer school) {
        userPostBrowsingHistoryTblService.removeAll(school);
        return Result.success();
    }
}
