//package com.remember.controller;
//
//import com.baomidou.mybatisplus.extension.toolkit.Db;
//import com.remember.domain.dto.JsoupGetMessageDTO;
//import com.remember.domain.po.IndustryInfoTbl;
//import com.remember.domain.po.PostSchoolTbl;
//import com.remember.domain.po.PostTbl;
//import com.remember.mapper.IndustryInfoTblMapper;
//import com.remember.mapper.PostSchoolTblMapper;
//import com.remember.mapper.PostTblMapper;
//import com.remember.result.Result;
//import com.remember.service.IIndustryInfoTblService;
//import com.remember.service.IPostTblService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import lombok.RequiredArgsConstructor;
//import net.sf.jsqlparser.expression.StringValue;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.PreDestroy;
//import java.net.URLEncoder;
//import java.nio.charset.StandardCharsets;
//import java.time.Duration;
//import java.util.Objects;
//
///*
// * @Author      : RememberKrystal
// * @Date        : 2024/8/21 10:28
// * @Description : Jsoup控制器
// */
//@RestController
//@RequestMapping("/jsoup")
//@Api(tags = "Jsoup控制器")
//public class JsoupController {
//
//    @Autowired(required = false)
//    PostTblMapper postTblMapper;
//
//    @Autowired
//    private PostSchoolTblMapper postSchoolTblMapper;
//
//    private int number;
//
//    private WebDriver driver;
//
//    // 构造方法，初始化WebDriver
//    public JsoupController(PostSchoolTblMapper postSchoolTblMapper) {
//        this.postSchoolTblMapper = postSchoolTblMapper;
//        System.setProperty("webdriver.chrome.driver", "D:\\software\\chromedriver-win64\\chromedriver.exe");
//
//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--remote-allow-origins=*");
//        options.setExperimentalOption("useAutomationExtension", false);
//        options.addArguments("--no-sandbox");
//        options.addArguments("--disable-dev-shm-usage");
//        options.addArguments("--disable-gpu");
//        options.addArguments("--headless");
//        options.addArguments("--window-size=1920,1200");
//        options.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36");
//
//        // 初始化WebDriver，只执行一次
//        this.driver = new ChromeDriver(options);
//    }
//
//    @PostMapping
//    @ApiOperation(value = "获取BOSS岗位信息,存入数据库")
//    public Result<?> getMessage(@RequestBody JsoupGetMessageDTO jsoupGetMessageDTO) {
//        number = 0; // 重置计数器
//        String keyword = jsoupGetMessageDTO.getKeyword();
//        IndustryInfoTbl industryInfoTbl = Db.lambdaQuery(IndustryInfoTbl.class).eq(IndustryInfoTbl::getIndustry, keyword).one();
//        if (industryInfoTbl == null) {
//            return Result.error("未找到该行业信息,请更换关键字！");
//        }
//        try {
//            // TODO： 可以模拟登录再进行操作 给出简单的示例操作
//            /*
//            // 打开登录页面
//            driver.get("https://example.com/login");
//
//            // 查找用户名和密码输入框并填写
//            WebElement usernameField = driver.findElement(By.name("username"));
//            WebElement passwordField = driver.findElement(By.name("password"));
//            WebElement loginButton = driver.findElement(By.name("submit"));
//
//            usernameField.sendKeys("your_username");
//            passwordField.sendKeys("your_password");
//            loginButton.click();
//
//            // 等待登录成功
//            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
//            wait.until(ExpectedConditions.urlContains("dashboard"));
//            // 登录成功后，执行其他操作
//             */
//
//            // 目标 URL
//            String baseUrl = "https://www.zhipin.com/web/geek/job?query=";
//            // 使用 URLEncoder 对中文参数进行编码
//            String encodedKeyword = URLEncoder.encode(jsoupGetMessageDTO.getKeyword(), String.valueOf(StandardCharsets.UTF_8));
//
//            String url = baseUrl + encodedKeyword +
//                    "&city=101270100" + // 城市固定成都
//                    "&page=" +
//                    jsoupGetMessageDTO.getPage();
//
//            driver.get(url);
//            // 等待页面中某个元素加载完成
//            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
//            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("search-job-result")));
//
//            // 获取页面内容
//            String pageSource = driver.getPageSource();
//
//            // 使用 Jsoup 解析页面内容
//            Document doc = Jsoup.parse(pageSource);
//
//            // 现在可以提取你需要的元素
//            Element ul = doc.selectFirst("ul.job-list-box");
//            Elements lis = null;
//            if (ul != null) {
//                lis = ul.children(); // 获取 ul 下的 子节点
//            }
//            if (lis != null) {
//                lis.forEach(li -> {
//                    Element jobName = li.selectFirst("div.job-card-body span.job-name");
//                    Element jobArea = li.selectFirst("div.job-card-body span.job-area");
//                    Element jobSalary = li.selectFirst("div.job-card-body span.salary");
//                    Elements childrenLis = li.select("ul.tag-list li");
//
//                    Element experience = childrenLis.first(); // 工作经验
//                    Element degree = childrenLis.get(1); // 学历
//
//                    Element companyName = li.selectFirst("div.job-card-right div.company-info a"); // 公司名称
//                    Element img = li.selectFirst("div.job-card-right div.company-logo img"); // 公司logo
//
//                    if (!Objects.isNull(jobName) && !Objects.isNull(jobArea) && !Objects.isNull(jobSalary)
//                            && !Objects.isNull(experience) && !Objects.isNull(degree)
//                            && !Objects.isNull(companyName) && !Objects.isNull(img)) {
//                        // 插入数据
//                        postSchoolTblMapper.insert(new PostSchoolTbl(jobName.text(), jobSalary.text(), jobSalary.text(), img.attr("src"),
//                                "成都", jobArea.text(), experience.text(), degree.text(), null, companyName.text(),
//                                1, industryInfoTbl.getIndustryId(), industryInfoTbl.getId(), 0, 0,1,2022));
//
//                        number++;
//                    }
//                });
//            }
//        } catch (Exception e) {
//            return Result.error(e.getMessage());
//        }
//        return Result.success("爬取" + number + "条数据成功！");
//    }
//
//    // 确保在应用关闭时关闭WebDriver
//    @PreDestroy
//    public void closeWebDriver() {
//        if (driver != null) {
//            driver.quit();
//        }
//    }
//}
