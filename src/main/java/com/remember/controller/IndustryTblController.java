package com.remember.controller;


import cn.hutool.json.JSONUtil;
import com.remember.domain.po.IndustryTbl;
import com.remember.result.Result;
import com.remember.service.IIndustryTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.codec.binary.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@RestController
@RequestMapping("/user/industry-tbl")
@RequiredArgsConstructor
@Api(tags = "父行业接口")
public class IndustryTblController {

    private final IIndustryTblService industryTblService;

    private final StringRedisTemplate stringRedisTemplate;

    private final static String INDUSTRY_KEY = "cache:industry:";
    /**
     * 查询全部父行业
     */
    @GetMapping("/getAll")
    @ApiOperation(value = "查询全部父行业")
    public Result<List<IndustryTbl>> getAllIndustry() {
        // 查询缓存是否有数据
        String industry = stringRedisTemplate.opsForValue().get(INDUSTRY_KEY);
        if (industry != null){
            // 有数据，直接返回
            List<IndustryTbl> list = JSONUtil.toList(industry, IndustryTbl.class);
            return Result.success(list);
        }
        // 没有数据，查询数据库
        List<IndustryTbl> list = industryTblService.list();
        // 存入Redis
        stringRedisTemplate.opsForValue().set(INDUSTRY_KEY, JSONUtil.toJsonStr(list));
        // 返回数据
        return Result.success(list);
    }
}
