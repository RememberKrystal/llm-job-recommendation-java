package com.remember.controller;


import com.remember.domain.po.EssayTbl;
import com.remember.result.Result;
import com.remember.service.IUserEssaySupportTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户推文点赞表 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/user-essay-support-tbl")
@RequiredArgsConstructor
@Api(tags = "用户推文点赞表相关接口")
public class UserEssaySupportTblController {
    private final IUserEssaySupportTblService userEssaySupportTblService;

    @GetMapping
    @ApiOperation("查询当前用户的推文点赞记录")
    public Result<List<EssayTbl>> getUserEssaySupport() {
        return userEssaySupportTblService.getUserEssaySupport();
    }

    @PostMapping
    @ApiOperation("点赞/取消点赞")
    public Result<?> supportEssay(Integer id) {
        return userEssaySupportTblService.supportEssay(id);
    }

}
