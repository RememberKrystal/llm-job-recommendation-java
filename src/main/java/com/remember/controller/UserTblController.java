package com.remember.controller;


import cn.hutool.json.JSONUtil;
import com.remember.context.BaseContext;
import com.remember.domain.dto.UserAttachDTO;
import com.remember.domain.dto.UserBasicInfoDTO;
import com.remember.domain.dto.UserRegisterDTO;
import com.remember.domain.po.UserTbl;
import com.remember.domain.vo.LoginSuccessVO;
import com.remember.result.Result;
import com.remember.service.IUserTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-07-27
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户管理")
@RequiredArgsConstructor
public class UserTblController {

    private final IUserTblService userTblService;

    private final StringRedisTemplate stringRedisTemplate;

    private static final String USER_LOGIN_KEY = "user:login:";

    /**
     * 获取验证码
     */
    @GetMapping("/sendSms")
    @ApiOperation(value = "发送验证码")
    public Result<String> sendSms(@ApiParam(value = "手机号") @RequestParam String phone) {
        return userTblService.sendSms(phone);
    }

    /**
     * 用户注册
     */
    @PostMapping("/register")
    @ApiOperation(value = "用户注册")
    public Result<String> register(@RequestBody UserRegisterDTO userRegisterDTO) {
        return userTblService.registerUser(userRegisterDTO);
    }

    /**
     * 用户登录
     */
    @GetMapping("/login")
    @ApiOperation(value = "用户登录")
    public Result<LoginSuccessVO> login(@ApiParam(value = "账号") @RequestParam String username,
                                        @ApiParam(value = "密码") @RequestParam String password) {
        return userTblService.login(username, password);
    }

    /**
     * 基础信息填写
     */
    @PostMapping("/basicInfo")
    @ApiOperation(value = "基础信息填写")
    public Result<String> basicInfo(@RequestBody UserBasicInfoDTO basicInfoDTO) {
        return userTblService.basicInfo(basicInfoDTO);
    }

    /**
     * 获取当前用户的基本信息
     */
    @GetMapping("/getBasicInfo")
    @ApiOperation(value = "获取当前用户的基本信息")
    public Result<UserTbl> getBasicInfo() {
        // 查询Redis
        String resultJSON = stringRedisTemplate.opsForValue().get(USER_LOGIN_KEY + BaseContext.getCurrentId());
        if (resultJSON != null) {
            return Result.success(JSONUtil.toBean(resultJSON, UserTbl.class));
        }
        // 不存在查询数据库
        UserTbl userTbl = userTblService.getById(BaseContext.getCurrentId());
        // 存入Redis
        stringRedisTemplate.opsForValue().set(USER_LOGIN_KEY + BaseContext.getCurrentId(), JSONUtil.toJsonStr(userTbl));
        // 返回数据
        return Result.success(userTbl);
    }

    /**
     * 修改用户的信息
     */
    @PostMapping("/update")
    @ApiOperation(value = "修改用户的基本信息")
    public Result<?> updateUser(@RequestBody UserBasicInfoDTO userBasicInfoDTO) {
        return userTblService.updateUser(userBasicInfoDTO);
    }

    /**
     * 修改密码
     */
    @PutMapping("/pas")
    @ApiOperation(value = "修改密码")
    public Result<?> updatePassword(@ApiParam(value = "旧密码") @RequestParam String oldPassword,
                                     @ApiParam(value = "新密码") @RequestParam String newPassword) {
        return userTblService.updatePassword(oldPassword, newPassword);
    }

}
