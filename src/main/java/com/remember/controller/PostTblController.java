package com.remember.controller;


import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.config.PostCacheConfig;
import com.remember.context.BaseContext;
import com.remember.domain.dto.GetPostDTO;
import com.remember.domain.po.PostTbl;
import com.remember.domain.po.UserInfoTbl;
import com.remember.domain.po.UserPostBrowsingHistoryTbl;
import com.remember.domain.po.UserPostCollectionTbl;
import com.remember.result.Result;
import com.remember.service.IPostTblService;
import com.remember.service.IUserPostBrowsingHistoryTblService;
import com.remember.utils.GetClientIpAddressUtil;
import com.remember.utils.UpdateUserViewsUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@RestController
@RequestMapping("/user/post-tbl")
@RequiredArgsConstructor
@Api(tags = "岗位相关接口")
public class PostTblController {

    private final IPostTblService postTblService;

    private final StringRedisTemplate stringRedisTemplate;

    private static final String KEY = "cache:post:"; // 根据期望公司子类型id查询的key

    @GetMapping("/getByExpectationTypeSonId")
    @ApiOperation(value = "根据当前用户的期望公司子类型id获取相关岗位")
    public Result<List<PostTbl>> getByExpectationTypeSonId(GetPostDTO getPostDTO) {
        // 获取到分页参数
        Integer page = getPostDTO.getPage();
        Integer size = getPostDTO.getSize();
        // 参数合法性检查
        if (page == null || page <= 0 || size == null || size <= 0) {
            return Result.error("Invalid page or size parameters.");
        }

        Long userId = BaseContext.getCurrentId();
        List<UserInfoTbl> list = Db.lambdaQuery(UserInfoTbl.class)
                .eq(UserInfoTbl::getUserId, userId)
                .list();

        // 查询Redis中是否有数据
        String key = KEY + list.getFirst().getExpectationTypeSonId();
        String resultJSON = stringRedisTemplate.opsForValue().get(key);
        if (resultJSON != null) {
            List<PostTbl> postTblList = JSONUtil.toList(resultJSON, PostTbl.class);
            // 对Redis中的数据进行分页
            int fromIndex = (page - 1) * size;
            int toIndex = Math.min(page * size, postTblList.size());

            if (fromIndex >= postTblList.size()) {
                return Result.success(Collections.emptyList());  // 防止分页越界
            }

            postTblList = postTblList.subList(fromIndex, toIndex);
            return Result.success(postTblList);
        }
        // 不存在查询数据库
        List<PostTbl> postTblList = postTblService.lambdaQuery()
                .eq(PostTbl::getIndustrySonId, list.getFirst().getExpectationTypeSonId())
                .list();
        // 缓存到Redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(postTblList));

        // 对Redis中的数据进行分页
        int fromIndex = (page - 1) * size;
        int toIndex = Math.min(page * size, postTblList.size());

        if (fromIndex >= postTblList.size()) {
            return Result.success(Collections.emptyList());  // 防止分页越界
        }

        postTblList = postTblList.subList(fromIndex, toIndex);

        return Result.success(postTblList);
    }

    @GetMapping
    @ApiOperation("根据子行业id查询相关岗位")
    public Result<List<PostTbl>> getByIndustrySonId(Integer industrySonId) {
        //查询Redis
        String key = KEY + industrySonId;
        String resultJSON = stringRedisTemplate.opsForValue().get(key);
        if (resultJSON != null) {
            List<PostTbl> postTblList = JSONUtil.toList(resultJSON, PostTbl.class);
            return Result.success(postTblList);
        }
        // 不存在查询数据库
        List<PostTbl> postTblList = postTblService.lambdaQuery()
                .eq(PostTbl::getIndustrySonId, industrySonId)
                .list();
        // 存入数据库
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(postTblList));
        // 返回数据
        return Result.success(postTblList);
    }

    @GetMapping("/all")
    @ApiOperation("条件查询岗位(搜索+分页)")
    public Result<List<PostTbl>> getAllPost(GetPostDTO getPostDTO) {
        return postTblService.getAllPost(getPostDTO);
    }

    private final PostCacheConfig postCacheConfig; // 用于记录ip访问量

    private final IUserPostBrowsingHistoryTblService userPostBrowsingHistoryTblService;

    private final ApplicationContext applicationContext; // 用于获取代理对象

    @GetMapping("/getById")
    @ApiOperation("根据岗位id查询岗位")
    public Result<PostTbl> getPostById(Integer id, HttpServletRequest request) {
        PostTbl postTbl = postTblService.lambdaQuery()
                .eq(PostTbl::getId, id)
                .one();
        if (postTbl == null) {
            return Result.error("岗位不存在");
        }
        // 4.增加每日浏览量
        // 获取ip
        String ipAddress = GetClientIpAddressUtil.getClientIp(request);
        postCacheConfig.ipCache().put(ipAddress + id, id); // 保证key不重复

        // 5.读取缓存的浏览量 + 数据库总的浏览量 = 实际浏览量
        Map<Integer, Integer> valueCount = new HashMap<>();

        for (String key : postCacheConfig.ipCache().keySet()) {
            Integer value = postCacheConfig.ipCache().get(key);
            valueCount.put(value, valueCount.getOrDefault(value, 0) + 1);
        }
        int size = valueCount.get(id);
        int views = postTblService.getById(id).getViews();
        // 实际浏览量
        postTbl.setViews(size + views);

        // 获取代理对象（事务）
        UpdateUserViewsUtil proxy = applicationContext.getBean(UpdateUserViewsUtil.class);
        proxy.updateUserViewAsync(id, BaseContext.getCurrentId(), 2);   // 调用异步方法,来处理用户浏览量操作

        // 查看当前用户是否收藏
        Long collectionCount = Db.lambdaQuery(UserPostCollectionTbl.class)
                .eq(UserPostCollectionTbl::getUserId, BaseContext.getCurrentId())
                .eq(UserPostCollectionTbl::getPostId, id)
                .eq(UserPostCollectionTbl::getSchool, 2)
                .count();
        if (collectionCount > 0) {
            postTbl.setCollection(true);// 该用户已收藏该岗位
        }

        // 清空valueCount
        valueCount.clear();

        // 7. 返回数据
        return Result.success(postTbl);
    }
}
