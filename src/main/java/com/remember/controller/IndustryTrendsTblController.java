package com.remember.controller;


import com.remember.domain.po.IndustryTrendsTbl;
import com.remember.result.Result;
import com.remember.service.IIndustryTrendsTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@RestController
@RequestMapping("/user/industry-trends-tbl")
@RequiredArgsConstructor
@Api(tags = "行业趋势相关接口")
public class IndustryTrendsTblController {

    private final IIndustryTrendsTblService industryTrendsTblService;

    @GetMapping
    @ApiOperation("根据父行业id查询行业趋势")
    public Result<List<IndustryTrendsTbl>> getIndustryTrends(@RequestParam("industryId") Integer industryId) {
        return industryTrendsTblService.getIndustryTrends(industryId);
    }
}
