package com.remember.controller;


import cn.hutool.json.JSONUtil;
import com.remember.context.BaseContext;
import com.remember.domain.dto.PublishEssayDTO;
import com.remember.domain.po.EssayTbl;
import com.remember.result.Result;
import com.remember.service.IEssayTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 推文表
 * 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@RestController
@RequestMapping("/user/essay-tbl")
@Api(tags = "推文相关接口")
@RequiredArgsConstructor
@Slf4j
public class EssayTblController {

    private final IEssayTblService essayTblService;

    @PostMapping
    @ApiOperation("发布推文")
    public Result<?> publishEssay(@RequestBody PublishEssayDTO publishEssayDTO) {
        return essayTblService.publishEssay(publishEssayDTO);
    }

    @DeleteMapping
    @ApiOperation("删除推文")
    public Result<?> deleteEssay(@RequestParam("id") Integer id) {
        // 查询是否存在推文，且是本人操作
        EssayTbl essayTbl = essayTblService.getById(id);
        if (essayTbl == null) {
            return Result.error("推文不存在");
        }
        if (essayTbl.getUserId() != BaseContext.getCurrentId().intValue()) {
            return Result.error("非本人操作");
        }
        // 执行删除操作
        essayTblService.removeById(id);
        return Result.success("删除成功！");
    }

    @PutMapping
    @ApiOperation("修改推文")
    public Result<?> updateEssay(@RequestBody EssayTbl essayTbl) {
        return essayTblService.updateEssay(essayTbl);
    }

    @GetMapping
    @ApiOperation("推文广场，查询全部的公开推文")
    public Result<List<EssayTbl>> getAllEssay() {
        return essayTblService.getAllEssay();
    }

    @GetMapping("/getByUserId")
    @ApiOperation("查询当前用户的推文")
    public Result<List<EssayTbl>> getByUserId() {
        List<EssayTbl> list = essayTblService.lambdaQuery()
                .eq(EssayTbl::getUserId, BaseContext.getCurrentId().intValue())
                .orderByDesc(EssayTbl::getWhenCreated)
                .list();
        for (EssayTbl essayTbl : list) {
            String image = essayTbl.getImage();
            List<String> imagesList = JSONUtil.toList(image, String.class);
            essayTbl.setImageList(imagesList); // 存入图片集合
        }
        return Result.success(list);
    }

    @GetMapping("/getById")
    @ApiOperation("根据推文id查询推文")
    public Result<EssayTbl> getEssayById(@RequestParam("id") Integer id, HttpServletRequest request) {
        return essayTblService.getEssayById(id, request);
    }
}
