package com.remember.controller;


import com.remember.domain.po.SearchHistoryTbl;
import com.remember.result.Result;
import com.remember.service.ISearchHistoryTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 搜索历史表 前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-11-21
 */
@RestController
@RequestMapping("/user/search")
@Api(tags = "搜索历史相关接口")
@RequiredArgsConstructor
public class SearchHistoryTblController {

    private final ISearchHistoryTblService searchHistoryTblService;
    @PostMapping
    @ApiOperation("添加搜索历史")
    public Result<String> addSearchHistory(@RequestBody SearchHistoryTbl searchHistoryTbl) {
        return searchHistoryTblService.addSearchHistory(searchHistoryTbl);
    }

    @GetMapping
    @ApiOperation("获取搜索历史")
    public Result<List<SearchHistoryTbl>> getSearchHistory() {
        return searchHistoryTblService.getSearchHistory();
    }

    @DeleteMapping
    @ApiOperation("删除搜索历史")
    public Result<String> deleteSearchHistory() {
        return searchHistoryTblService.deleteSearchHistory();
    }
}
