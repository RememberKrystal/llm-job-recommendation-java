package com.remember.controller;


import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.po.AiReplyTbl;
import com.remember.result.Result;
import com.remember.service.IAiReplyTblService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@RestController
@RequestMapping("/user/ai-reply-tbl")
@RequiredArgsConstructor
@Api(tags = "AI回复接口")
public class AiReplyTblController {

    private final IAiReplyTblService aiReplyTblService;

    @GetMapping("/get")
    @ApiOperation(value = "获取AI对话信息")
    public Result<List<AiReplyTbl>> getAiReply(){
        List<AiReplyTbl> list = Db.lambdaQuery(AiReplyTbl.class)
                .eq(AiReplyTbl::getUserId, BaseContext.getCurrentId().intValue()).orderByAsc(AiReplyTbl::getWhenCreated).list();
        return Result.success(list);
    }

    @PostMapping("/add")
    @ApiOperation(value = "新增AI对话信息")
    public Result<?> addAiReply(@RequestBody AiReplyTbl aiReplyTbl){
        aiReplyTbl.setUserId(BaseContext.getCurrentId().intValue());
        aiReplyTbl.setWhenCreated(LocalDateTime.now());
        return aiReplyTblService.save(aiReplyTbl) ? Result.success() : Result.error("新增失败");
    }
}
