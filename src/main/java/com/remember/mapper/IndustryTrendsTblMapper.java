package com.remember.mapper;

import com.remember.domain.po.IndustryTrendsTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface IndustryTrendsTblMapper extends BaseMapper<IndustryTrendsTbl> {

}
