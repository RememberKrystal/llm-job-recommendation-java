package com.remember.mapper;

import com.remember.domain.po.UserInfoTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface UserInfoTblMapper extends BaseMapper<UserInfoTbl> {

}
