package com.remember.mapper;

import com.remember.domain.po.UserEssayBrowsingHistoryTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户推文浏览记录表 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface UserEssayBrowsingHistoryTblMapper extends BaseMapper<UserEssayBrowsingHistoryTbl> {

}
