package com.remember.mapper;

import com.remember.domain.po.PostTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface PostTblMapper extends BaseMapper<PostTbl> {
    @Select("SELECT * FROM post_tbl WHERE id IN (SELECT post_id FROM user_post_collection_tbl WHERE user_id = #{userId} AND school = 2)")
    List<PostTbl> findUserFavorites(Long userId);

    @Select("SELECT * FROM post_tbl WHERE id IN (SELECT post_id FROM user_post_browsing_history_tbl WHERE user_id = #{userId} AND school = 2)")
    List<PostTbl> findUserViews(Long userId);

    @Select("<script>" +
            "SELECT * FROM post_tbl WHERE industry_id IN " +
            "<foreach item='id' collection='list' open='(' separator=',' close=')'>#{id}</foreach>" +
            "</script>")
    List<PostTbl> findPostsByIndustryIds(@Param("list") List<Integer> industryIds);

    @Select("SELECT * FROM post_tbl WHERE city = #{city} AND experience = #{experience}")
    List<PostTbl> findPostsByCityAndExperience(@Param("city") String city, @Param("experience") String experience);
}
