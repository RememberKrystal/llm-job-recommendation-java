package com.remember.mapper;

import com.remember.domain.po.UserTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-07-27
 */
public interface UserTblMapper extends BaseMapper<UserTbl> {

}
