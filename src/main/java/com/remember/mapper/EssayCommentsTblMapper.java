package com.remember.mapper;

import com.remember.domain.po.EssayCommentsTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 推文一级评论表 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface EssayCommentsTblMapper extends BaseMapper<EssayCommentsTbl> {

}
