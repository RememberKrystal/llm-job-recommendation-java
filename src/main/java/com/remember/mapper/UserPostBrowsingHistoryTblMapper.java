package com.remember.mapper;

import com.remember.domain.po.UserPostBrowsingHistoryTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户岗位浏览记录 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface UserPostBrowsingHistoryTblMapper extends BaseMapper<UserPostBrowsingHistoryTbl> {

}
