package com.remember.mapper;

import com.remember.domain.po.UserEssayCollectionTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户推文收藏表 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface UserEssayCollectionTblMapper extends BaseMapper<UserEssayCollectionTbl> {

}
