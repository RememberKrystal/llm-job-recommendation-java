package com.remember.mapper;

import com.remember.domain.po.PostSchoolTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 秋招春招信息表
 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface PostSchoolTblMapper extends BaseMapper<PostSchoolTbl> {

}
