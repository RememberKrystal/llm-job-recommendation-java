package com.remember.mapper;

import com.remember.domain.po.SpecializedTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface SpecializedTblMapper extends BaseMapper<SpecializedTbl> {

}
