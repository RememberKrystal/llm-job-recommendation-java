package com.remember.mapper;

import com.remember.domain.po.EssayTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 推文表
 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface EssayTblMapper extends BaseMapper<EssayTbl> {

}
