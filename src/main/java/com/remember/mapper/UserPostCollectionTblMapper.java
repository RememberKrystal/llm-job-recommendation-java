package com.remember.mapper;

import com.remember.domain.po.UserPostCollectionTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户收藏岗位信息表 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface UserPostCollectionTblMapper extends BaseMapper<UserPostCollectionTbl> {

}
