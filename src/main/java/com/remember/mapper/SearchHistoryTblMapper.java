package com.remember.mapper;

import com.remember.domain.po.SearchHistoryTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 搜索历史表 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-11-21
 */
public interface SearchHistoryTblMapper extends BaseMapper<SearchHistoryTbl> {

}
