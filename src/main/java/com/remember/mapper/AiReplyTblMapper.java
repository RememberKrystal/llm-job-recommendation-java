package com.remember.mapper;

import com.remember.domain.po.AiReplyTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface AiReplyTblMapper extends BaseMapper<AiReplyTbl> {

}
