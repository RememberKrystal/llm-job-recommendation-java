package com.remember.mapper;

import com.remember.domain.po.UserEssaySupportTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户推文点赞表 Mapper 接口
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface UserEssaySupportTblMapper extends BaseMapper<UserEssaySupportTbl> {

}
