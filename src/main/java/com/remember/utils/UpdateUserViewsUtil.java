package com.remember.utils;

import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.domain.po.UserPostBrowsingHistoryTbl;
import com.remember.service.IUserPostBrowsingHistoryTblService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/9/22 12:27
 * @Description : 修改用户校园岗位/ 非校园岗位的浏览量
 */
@RequiredArgsConstructor
@Component // 交给Spring容器管理
public class UpdateUserViewsUtil {

    private final IUserPostBrowsingHistoryTblService userPostBrowsingHistoryTblService;

    public void addUserViews(Integer id, Long userId , int school) {
        // 存入新数据
        UserPostBrowsingHistoryTbl userPostBrowsingHistoryTbl = new UserPostBrowsingHistoryTbl();
        userPostBrowsingHistoryTbl.setPostId(id);
        userPostBrowsingHistoryTbl.setUserId(userId.intValue());
        userPostBrowsingHistoryTbl.setSchool(school); // 2 不是校招岗位
        userPostBrowsingHistoryTbl.setWhenCreated(LocalDateTime.now());
        userPostBrowsingHistoryTblService.save(userPostBrowsingHistoryTbl);
    }


    @Async // 标注该方法异步执行
    @Transactional
    public void updateUserViewAsync(Integer id, Long userId,int school) {
        UserPostBrowsingHistoryTbl one = Db.lambdaQuery(UserPostBrowsingHistoryTbl.class)
                .eq(UserPostBrowsingHistoryTbl::getUserId, userId.intValue())
                .eq(UserPostBrowsingHistoryTbl::getPostId, id)
                .eq(UserPostBrowsingHistoryTbl::getSchool, school) // 2 不是校招岗位
                .one();
        if (one == null) {
            addUserViews(id, userId,school); // 新增
        } else {
            one.setWhenCreated(LocalDateTime.now());
            userPostBrowsingHistoryTblService.updateById(one); // 更新时间
        }

        Long count = Db.lambdaQuery(UserPostBrowsingHistoryTbl.class)
                .eq(UserPostBrowsingHistoryTbl::getUserId, userId.intValue())
                .count();

        if (count > 50) {
            Db.lambdaUpdate(UserPostBrowsingHistoryTbl.class)
                    .orderByAsc(UserPostBrowsingHistoryTbl::getWhenCreated)
                    .last("limit 1")
                    .remove(); // 删除最旧的记录
        }
    }
}
