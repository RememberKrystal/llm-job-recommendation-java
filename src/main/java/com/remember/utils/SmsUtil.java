package com.remember.utils;

import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.Common;
import com.aliyun.teautil.models.RuntimeOptions;
import com.remember.properties.AliSmsProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/7/28 14:37
 * @Description : 短信验证码
 */
@RequiredArgsConstructor
@Component
public class SmsUtil {

    private final AliSmsProperties aliSmsProperties;

    private final RedisTemplate<String, Object> redisTemplate;

    /**
     * 发送验证码
     */
    public void sendSms(String phone) throws Exception {
        // 随机生成四位验证码
        Random random = new Random();
        String code = String.valueOf(random.nextInt(8999) + 1000);

        // 存储验证码到 Redis 并设置过期时间为1分钟 key:phone value:code
        redisTemplate.opsForValue().set(phone, code, 1, TimeUnit.MINUTES);

        // 转化为JSON
        String templateParam = "{\"code\":\"" + code + "\"}";

        Config config = new Config()
                .setAccessKeyId(aliSmsProperties.getAccessKeyId())
                .setAccessKeySecret(aliSmsProperties.getAccessKeySecret());
        // Endpoint 请参考 https://api.aliyun.com/product/Dysmsapi
        config.endpoint = aliSmsProperties.getEndpoint();
        Client client = new Client(config);
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName(aliSmsProperties.getSignName()) // 短信签名名称。
                .setTemplateCode(aliSmsProperties.getTemplateCode()) // 短信模板Code
                .setTemplateParam(templateParam)
                .setPhoneNumbers(phone);
        try {
            // 复制代码运行请自行打印 API 的返回值
            client.sendSmsWithOptions(sendSmsRequest, new RuntimeOptions());
        } catch (TeaException error) {
            // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
            // 错误 message
            System.out.println(error.getMessage());
            // 诊断地址
            System.out.println(error.getData().get("Recommend"));
            Common.assertAsString(error.message);

            throw new Exception("发送短信失败" + error.getMessage());
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
            // 错误 message
            System.out.println(error.getMessage());
            // 诊断地址
            System.out.println(error.getData().get("Recommend"));
            Common.assertAsString(error.message);

            throw new Exception("发送短信失败" + error.getMessage());
        }
    }

    /**
     * 获取验证码
     */
    public String getCode(String phone) {
        return (String) redisTemplate.opsForValue().get(phone);
    }
}
