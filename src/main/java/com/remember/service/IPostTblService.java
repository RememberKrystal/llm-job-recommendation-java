package com.remember.service;

import com.remember.domain.dto.GetPostDTO;
import com.remember.domain.po.PostTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface IPostTblService extends IService<PostTbl> {

    Result<List<PostTbl>> getAllPost(GetPostDTO getPostDTO);
}
