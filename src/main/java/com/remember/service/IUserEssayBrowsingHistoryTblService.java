package com.remember.service;

import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserEssayBrowsingHistoryTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 * 用户推文浏览记录表 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IUserEssayBrowsingHistoryTblService extends IService<UserEssayBrowsingHistoryTbl> {

    Result<List<EssayTbl>> getUserEssayBrowsingHistory();

    Result<?> deleteUserEssayBrowsingHistory();

}
