package com.remember.service;

import com.remember.domain.dto.UserAttachDTO;
import com.remember.domain.po.UserInfoTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface IUserInfoTblService extends IService<UserInfoTbl> {

    Result<String> attachInfo(UserAttachDTO userAttachDTO);

    Result<UserInfoTbl> getInfo();

    Result<?> updateAttachInfo(UserAttachDTO userAttachDTO);
}
