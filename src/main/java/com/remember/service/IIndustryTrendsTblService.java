package com.remember.service;

import com.remember.domain.po.IndustryTrendsTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface IIndustryTrendsTblService extends IService<IndustryTrendsTbl> {

    Result<List<IndustryTrendsTbl>> getIndustryTrends(Integer industryId);
}
