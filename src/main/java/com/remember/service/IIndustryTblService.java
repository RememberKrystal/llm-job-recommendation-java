package com.remember.service;

import com.remember.domain.po.IndustryTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
public interface IIndustryTblService extends IService<IndustryTbl> {

}
