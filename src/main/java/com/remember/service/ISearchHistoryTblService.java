package com.remember.service;

import com.remember.domain.po.SearchHistoryTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 * 搜索历史表 服务类
 * </p>
 *
 * @author remember
 * @since 2024-11-21
 */
public interface ISearchHistoryTblService extends IService<SearchHistoryTbl> {

    Result<String> addSearchHistory(SearchHistoryTbl searchHistoryTbl);

    Result<List<SearchHistoryTbl>> getSearchHistory();

    Result<String> deleteSearchHistory();
}
