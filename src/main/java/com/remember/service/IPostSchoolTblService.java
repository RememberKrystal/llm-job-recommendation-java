package com.remember.service;

import com.remember.domain.po.PostSchoolTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 秋招春招信息表
 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IPostSchoolTblService extends IService<PostSchoolTbl> {

    Result<List<PostSchoolTbl>> getAllPostSchool(int page, int size);

    Result<PostSchoolTbl> getPostSchoolGetById(Integer id, HttpServletRequest request);

    Result<List<PostSchoolTbl>> getByYearOrSchool(Integer year, Integer school,int page, int size);
}
