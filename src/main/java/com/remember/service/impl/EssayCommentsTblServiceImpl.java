package com.remember.service.impl;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.dto.PublishEssayCommentsDTO;
import com.remember.domain.po.EssayCommentsTbl;
import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserTbl;
import com.remember.domain.vo.EssayCommentsVO;
import com.remember.mapper.EssayCommentsTblMapper;
import com.remember.result.Result;
import com.remember.service.IEssayCommentsTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 推文一级评论表 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
@RequiredArgsConstructor
public class EssayCommentsTblServiceImpl extends ServiceImpl<EssayCommentsTblMapper, EssayCommentsTbl> implements IEssayCommentsTblService {

    @Override
    public Result<?> publishEssayComments(PublishEssayCommentsDTO publishEssayCommentsDTO) {
        // 查看推文是否存在
        Integer essayId = publishEssayCommentsDTO.getEssayId();
        if (!Db.lambdaQuery(EssayTbl.class)
                .eq(EssayTbl::getId, essayId)
                .exists()) {
            return Result.error("推文不存在！");
        }
        // 将图片集合转为JSON字符串
        List<String> images = publishEssayCommentsDTO.getImages();
        String imageJSON = JSONUtil.toJsonStr(images);

        EssayCommentsTbl essayCommentsTbl = new EssayCommentsTbl();
        essayCommentsTbl.setEssayId(publishEssayCommentsDTO.getEssayId());
        essayCommentsTbl.setUserId(BaseContext.getCurrentId().intValue());
        essayCommentsTbl.setComments(publishEssayCommentsDTO.getComments());
        essayCommentsTbl.setImages(imageJSON);
        essayCommentsTbl.setWhenCreated(LocalDateTime.now());
        // 保存评论
        save(essayCommentsTbl);
        return Result.success("发布评论成功！");
    }

    @Override
    public Result<List<EssayCommentsVO>> getEssayComments(Integer essayId) {
        // 根据推文id查询一级评论
        List<EssayCommentsTbl> list = lambdaQuery()
                .eq(EssayCommentsTbl::getEssayId, essayId)
                .orderByDesc(EssayCommentsTbl::getWhenCreated)
                .list();
        List<EssayCommentsVO> essayCommentsVOList = new ArrayList<>();

        for (EssayCommentsTbl essayCommentsTbl : list) {
            EssayCommentsVO essayCommentsVO = new EssayCommentsVO();
            // 是否是 本人评论
            essayCommentsVO.setSelf(BaseContext.getCurrentId().intValue() == essayCommentsTbl.getUserId());
            essayCommentsVO.setUsername(getUserName(essayCommentsTbl.getUserId()));
            // 将图片JSON转换为集合
            essayCommentsVO.setImages(JSONUtil.toList(essayCommentsTbl.getImages(), String.class));
            essayCommentsVO.setComments(essayCommentsTbl.getComments());
            essayCommentsVO.setWhenCreated(essayCommentsTbl.getWhenCreated());
            essayCommentsVO.setUserId(essayCommentsTbl.getUserId());
            essayCommentsVO.setEssayId(essayCommentsTbl.getEssayId());
            // 存入集合
            essayCommentsVOList.add(essayCommentsVO);
        }
        // 返回数据
        return Result.success(essayCommentsVOList);
    }


    private String getUserName(Integer userId){
        return Db.lambdaQuery(UserTbl.class)
                .eq(UserTbl::getId, userId)
                .one().getUsername();
    }
}
