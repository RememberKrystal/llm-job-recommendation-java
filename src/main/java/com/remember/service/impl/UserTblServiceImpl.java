package com.remember.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.remember.constant.JwtClaimsConstant;
import com.remember.context.BaseContext;
import com.remember.domain.dto.UserAttachDTO;
import com.remember.domain.dto.UserBasicInfoDTO;
import com.remember.domain.dto.UserRegisterDTO;
import com.remember.domain.po.UserTbl;
import com.remember.domain.vo.LoginSuccessVO;
import com.remember.mapper.UserTblMapper;
import com.remember.properties.JwtProperties;
import com.remember.result.Result;
import com.remember.service.IUserTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.remember.utils.JwtUtil;
import com.remember.utils.SmsUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-07-27
 */
@Service
@RequiredArgsConstructor
public class UserTblServiceImpl extends ServiceImpl<UserTblMapper, UserTbl> implements IUserTblService {

    private final SmsUtil smsUtil;

    private final JwtProperties jwtProperties;

    private final StringRedisTemplate stringRedisTemplate;

    private static final String USER_LOGIN_KEY = "user:login:";

    @Override
    public Result<String> registerUser(UserRegisterDTO userRegisterDTO) {
        UserTbl user = lambdaQuery()
                .eq(UserTbl::getUsername, userRegisterDTO.getUsername())
                .one();
        if (user != null){
            return Result.error("用户名已存在");
        }
        UserTbl u = lambdaQuery()
                .eq(UserTbl::getPhone, userRegisterDTO.getPhone())
                .one();
        if (u != null){
            return Result.error("一个手机号只能注册一个用户");
        }
        // 获取验证码
        String code = smsUtil.getCode(userRegisterDTO.getPhone());
        if (code == null || !code.equals(userRegisterDTO.getCode())){
            return Result.error("验证码错误！");
        }
        UserTbl userTbl = new UserTbl();
        // 对象属性拷贝
        BeanUtils.copyProperties(userRegisterDTO, userTbl);
        userTbl.setDeleted("n");
        userTbl.setState(1);
        userTbl.setWhenCreated(LocalDateTime.now());
        save(userTbl);
        return Result.success("新增成功！");
    }

    @Override
    public Result<String> sendSms(String phone) {
        UserTbl userTbl = lambdaQuery()
                .eq(UserTbl::getPhone, phone)
                .one();
        if (userTbl != null){
            return Result.error("一个手机号只能注册一个用户");
        }
        try {
            // TODO 安全问题：要判断是否是用户恶意刷
            smsUtil.sendSms(phone); // 发送验证码
            return Result.success("发送成功！");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Result<LoginSuccessVO> login(String username, String password) {
        UserTbl userTbl = lambdaQuery()
                .eq(UserTbl::getUsername, username)
                .eq(UserTbl::getPassword, password)
                .one();
        if (userTbl == null){
            return Result.error("用户名或密码错误！");
        }
        //登录成功后，生成jwt令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.USER_ID, userTbl.getId());
        String token = JwtUtil.createJWT(
                jwtProperties.getUserSecretKey(),
                jwtProperties.getUserTtl(),
                claims);
        LoginSuccessVO loginSuccessVO = new LoginSuccessVO();
        loginSuccessVO.setUser(userTbl);
        loginSuccessVO.setToken(token);
        // 存入token
        userTbl.setToken(token);
        updateById(userTbl);
        return Result.success(loginSuccessVO);
    }

    @Override
    public Result<String> basicInfo(UserBasicInfoDTO basicInfoDTO) {
        UserTbl userTbl = getById(BaseContext.getCurrentId());
        BeanUtil.copyProperties(basicInfoDTO, userTbl);
        updateById(userTbl);
        return Result.success("新增信息成功！");
    }

    @Override
    public Result<?> updateUser(UserBasicInfoDTO userBasicInfoDTO) {
        UserTbl user = new UserTbl();
        // 查询缓存
        String result = stringRedisTemplate.opsForValue().get(USER_LOGIN_KEY + BaseContext.getCurrentId());
        if (result != null){
            user = JSONUtil.toBean(result, UserTbl.class);
        }
        // 不存在，查询数据库
        if (user == null){
            user = query().eq("id", BaseContext.getCurrentId()).one();
        }
        BeanUtils.copyProperties(userBasicInfoDTO, user);
        // 执行修改操作
        updateById(user);
        // 删除缓存
        stringRedisTemplate.delete(USER_LOGIN_KEY + BaseContext.getCurrentId());

        return Result.success("修改成功！");
    }

    @Override
    public Result<?> updatePassword(String oldPassword, String newPassword) {
        // 查看旧密码是否正确
        int userId = BaseContext.getCurrentId().intValue();
        UserTbl userTbl = this.getById(userId);
        if (!userTbl.getPassword().equals(oldPassword)){
            return Result.error("旧密码错误！");
        }
        userTbl.setPassword(newPassword);
        updateById(userTbl);
        stringRedisTemplate.delete(USER_LOGIN_KEY + userId);
        return Result.success("修改成功！");
    }
}
