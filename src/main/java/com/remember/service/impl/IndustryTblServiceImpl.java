package com.remember.service.impl;

import com.remember.domain.po.IndustryTbl;
import com.remember.mapper.IndustryTblMapper;
import com.remember.service.IIndustryTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Service
public class IndustryTblServiceImpl extends ServiceImpl<IndustryTblMapper, IndustryTbl> implements IIndustryTblService {

}
