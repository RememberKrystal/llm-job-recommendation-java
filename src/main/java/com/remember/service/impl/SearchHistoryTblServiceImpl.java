package com.remember.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.po.SearchHistoryTbl;
import com.remember.mapper.SearchHistoryTblMapper;
import com.remember.result.Result;
import com.remember.service.ISearchHistoryTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 搜索历史表 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-11-21
 */
@Service
public class SearchHistoryTblServiceImpl extends ServiceImpl<SearchHistoryTblMapper, SearchHistoryTbl> implements ISearchHistoryTblService {

    @Override
    @Transactional
    public Result<String> addSearchHistory(SearchHistoryTbl searchHistoryTbl) {
        // 查询是否存在重复的历史记录
        SearchHistoryTbl one = Db.lambdaQuery(SearchHistoryTbl.class)
                .eq(SearchHistoryTbl::getUserId, BaseContext.getCurrentId().intValue())
                .eq(SearchHistoryTbl::getHistory, searchHistoryTbl.getHistory())
                .one();
        if (one != null){
            // 存在，则修改时间
            one.setWhenCreated(LocalDateTime.now());
            updateById(one);
            return Result.success();
        }
        // 查询是否超出十条
        Long count = Db.lambdaQuery(SearchHistoryTbl.class)
                .eq(SearchHistoryTbl::getUserId, BaseContext.getCurrentId().intValue())
                .count();
        if (count >= 10){
            // 删除最早的一条
            removeById(Db.lambdaQuery(SearchHistoryTbl.class)
                    .eq(SearchHistoryTbl::getUserId, BaseContext.getCurrentId().intValue())
                    .orderByAsc(SearchHistoryTbl::getWhenCreated) // 升序
                    .last("limit 1")
                    .one());
        }
        // 新增数据
        searchHistoryTbl.setWhenCreated(LocalDateTime.now());
        searchHistoryTbl.setUserId(BaseContext.getCurrentId().intValue());
        save(searchHistoryTbl);
        return Result.success();
    }

    @Override
    public Result<List<SearchHistoryTbl>> getSearchHistory() {
        List<SearchHistoryTbl> list = lambdaQuery()
                .eq(SearchHistoryTbl::getUserId, BaseContext.getCurrentId().intValue())
                .orderByDesc(SearchHistoryTbl::getWhenCreated) // 降序
                .list();
        return Result.success(list);
    }

    @Override
    public Result<String> deleteSearchHistory() {
        LambdaQueryWrapper<SearchHistoryTbl> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SearchHistoryTbl::getUserId, BaseContext.getCurrentId().intValue());
        remove(wrapper);
        return Result.success("清理成功！");
    }
}
