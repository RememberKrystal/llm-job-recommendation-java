package com.remember.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.dto.UserAttachDTO;
import com.remember.domain.po.*;
import com.remember.mapper.UserInfoTblMapper;
import com.remember.result.Result;
import com.remember.service.IUserInfoTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.remember.service.IUserTblService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Service
@RequiredArgsConstructor
public class UserInfoTblServiceImpl extends ServiceImpl<UserInfoTblMapper, UserInfoTbl> implements IUserInfoTblService {

    private final StringRedisTemplate stringRedisTemplate;

    private static final String USER_INFO_KEY = "user:info:";

    @Override
    public Result<String> attachInfo(UserAttachDTO userAttachDTO) {
        UserInfoTbl userInfoTbl = new UserInfoTbl();
        BeanUtil.copyProperties(userAttachDTO, userInfoTbl);
        userInfoTbl.setUserId(Integer.valueOf(String.valueOf(BaseContext.getCurrentId()))); // 存入用户id
        save(userInfoTbl);

        // 存入user表中
        Db.lambdaUpdate(UserTbl.class)
                .eq(UserTbl::getId, Integer.valueOf(String.valueOf(BaseContext.getCurrentId())))
                .set(UserTbl::getInformationId, userInfoTbl.getId())
                .update();
        return Result.success("补充信息成功！");
    }

    // 查询当前登录用户的附加信息
    @Override
    public Result<UserInfoTbl> getInfo() {
        Long userId = BaseContext.getCurrentId();
        // 查询Redis
        String info = stringRedisTemplate.opsForValue().get(USER_INFO_KEY + userId);
        if (info != null){
            // 有数据，直接返回
            return Result.success(JSONUtil.toBean(info, UserInfoTbl.class));
        }
        // 不存在，查询数据库
        UserInfoTbl userInfoTbl = lambdaQuery()
                .eq(UserInfoTbl::getUserId, userId)
                .one();
        if (userInfoTbl == null){
            return Result.error("用户信息不存在！");
        }
        // 用户信息存在
        List<SpecializedTbl> list = Db.lambdaQuery(SpecializedTbl.class)
                .eq(userInfoTbl.getSpecializedId() != null, SpecializedTbl::getId, userInfoTbl.getSpecializedId())
                .list();
        if (list != null){
            String name = list.getFirst().getName(); // 专业名称
            userInfoTbl.setSpecialized(name);
        }
        List<IndustryTbl> industryTblList = Db.lambdaQuery(IndustryTbl.class).list(); // 父行业
        List<IndustryInfoTbl> industryInfoTblList = Db.lambdaQuery(IndustryInfoTbl.class).list(); // 子行业

        // 实习公司父类型
        if (userInfoTbl.getFirmTypeId() != null){
            industryTblList.forEach(v->{
                if (Objects.equals(userInfoTbl.getFirmTypeId(), v.getId())){
                    userInfoTbl.setFirmType(v.getIndustryName());
                }
            });
        }
        // 实习公司子类型
        if (userInfoTbl.getFirmTypeSonId() != null){
            industryInfoTblList.forEach(v->{
                if (Objects.equals(userInfoTbl.getFirmTypeSonId(), v.getId())){
                    userInfoTbl.setFirmTypeSon(v.getIndustry());
                }
            });
        }
        // 期望公司父类型
        if (userInfoTbl.getExpectationTypeId() != null){
            industryTblList.forEach(v->{
                if (Objects.equals(userInfoTbl.getExpectationTypeId(), v.getId())){
                    userInfoTbl.setExpectationType(v.getIndustryName());
                }
            });
        }
        // 期望公司子类型
        if (userInfoTbl.getExpectationTypeSonId() != null){
            industryInfoTblList.forEach(v->{
                if (Objects.equals(userInfoTbl.getExpectationTypeSonId(), v.getId())){
                    userInfoTbl.setExpectationTypeSon(v.getIndustry());
                }
            });
        }
        // 存入Redis
        stringRedisTemplate.opsForValue().set(USER_INFO_KEY + userId, JSONUtil.toJsonStr(userInfoTbl));

        return Result.success(userInfoTbl);
    }

    @Override
    public Result<?> updateAttachInfo(UserAttachDTO userAttachDTO) {
        // 查询当前用户的附加信息
        UserInfoTbl userInfoTbl = query().eq("user_id", BaseContext.getCurrentId()).one();
        // 拷贝
        BeanUtil.copyProperties(userAttachDTO, userInfoTbl);
        // 执行修改
        updateById(userInfoTbl);
        // 删除缓存
        stringRedisTemplate.delete(USER_INFO_KEY + BaseContext.getCurrentId());
        return Result.success("修改成功！");
    }
}
