package com.remember.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.config.EssayCacheConfig;
import com.remember.context.BaseContext;
import com.remember.domain.dto.PublishEssayDTO;
import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserEssayBrowsingHistoryTbl;
import com.remember.domain.po.UserEssayCollectionTbl;
import com.remember.domain.po.UserEssaySupportTbl;
import com.remember.mapper.EssayTblMapper;
import com.remember.result.Result;
import com.remember.service.IEssayTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.remember.service.IUserEssayBrowsingHistoryTblService;
import com.remember.utils.GetClientIpAddressUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 推文表
 * 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
@RequiredArgsConstructor
public class EssayTblServiceImpl extends ServiceImpl<EssayTblMapper, EssayTbl> implements IEssayTblService {

    @Override
    public Result<?> publishEssay(PublishEssayDTO publishEssayDTO) {
        EssayTbl essayTbl = new EssayTbl();
        // 对象属性拷贝
        BeanUtil.copyProperties(publishEssayDTO, essayTbl);
        // 存入其他的信息
        List<String> imageList = publishEssayDTO.getImageList();
        String jsonStr = JSONUtil.toJsonStr(imageList);

        essayTbl.setImage(jsonStr);
        essayTbl.setUserId(BaseContext.getCurrentId().intValue());
        essayTbl.setWhenCreated(LocalDateTime.now());
        essayTbl.setViews(0); // 初始化浏览量等数据
        essayTbl.setSupportNumber(0);
        essayTbl.setCollectionNumber(0);
        // 保存数据
        return save(essayTbl) ? Result.success("发布成功！") : Result.error("发布失败");
    }

    @Override
    public Result<?> updateEssay(EssayTbl essayTbl) {
        // 执行修改
        return updateById(essayTbl) ? Result.success("修改成功！") : Result.error("修改失败");
    }

    @Override
    public Result<List<EssayTbl>> getAllEssay() {
        List<EssayTbl> list = lambdaQuery()
                .eq(EssayTbl::getState, 1) // 公开
                .orderByDesc(EssayTbl::getWhenCreated) // 时间排序
                .list();

        for (EssayTbl essayTbl : list) {
            String image = essayTbl.getImage();
            List<String> imagesList = JSONUtil.toList(image, String.class);
            essayTbl.setImageList(imagesList); // 存入图片集合
        }
        return Result.success(list);
    }

    private final EssayCacheConfig essayCacheConfig;

    private final ApplicationContext applicationContext; // spring上下文，用于获取代理对象

    @Override
    public Result<EssayTbl> getEssayById(Integer id, HttpServletRequest request) {
        // 1.查询推文是否存在
        EssayTbl essayTbl = getById(id);
        if (essayTbl == null) {
            return Result.error("推文不存在");
        }
        // 2.增加推文的浏览量(IP区分)
        String ipAddress = GetClientIpAddressUtil.getClientIp(request);
        essayCacheConfig.ipCache().put(ipAddress + id, id); // 保证key值不重复（相同IP浏览不同推文，Key不重复）

        // 3.获取该推文的实际浏览量 = 缓存的浏览量 + 数据库的浏览量
        Map<Integer, Integer> valueCount = new HashMap<>();
        for (String key : essayCacheConfig.ipCache().keySet()) {
            Integer value = essayCacheConfig.ipCache().get(key);
            valueCount.put(value, valueCount.getOrDefault(value, 0) + 1);
        }
        int size = valueCount.get(id);
        int views = getById(id).getViews();
        // 4.存入实际浏览量
        essayTbl.setViews(size + views);

        // 5.新增用户的推文浏览记录（这里采用异步线程）
        // 获取代理对象
        EssayTblServiceImpl bean = applicationContext.getBean(EssayTblServiceImpl.class);
        bean.updateUserEssayBrowsingHistory(id,BaseContext.getCurrentId());

        // 6. 查看用户是否点赞该推文
        Long count = Db.lambdaQuery(UserEssaySupportTbl.class)
                .eq(UserEssaySupportTbl::getUserId, BaseContext.getCurrentId().intValue())
                .eq(UserEssaySupportTbl::getEssayId, id)
                .count();
        if (count > 0) {
            essayTbl.setSupport(true);// 该用户已点赞该推文
        }

        // 7. 查看用户是否收藏该推文
        Long count1 = Db.lambdaQuery(UserEssayCollectionTbl.class)
                .eq(UserEssayCollectionTbl::getUserId, BaseContext.getCurrentId().intValue())
                .eq(UserEssayCollectionTbl::getEssayId, id)
                .count();
        if (count1 > 0) {
            essayTbl.setCollection(true);// 该用户已收藏该推文
        }

        essayTbl.setImageList(JSONUtil.toList(essayTbl.getImage(), String.class)); // 存入图片资源集合（后端转换，避免前端转换）

        // 清空valueCount
        valueCount.clear();
        // 8. 返回数据
        return Result.success(essayTbl);
    }

    private final IUserEssayBrowsingHistoryTblService userEssayBrowsingHistoryTblService;
    @Async // 标识为异步线程
    public void updateUserEssayBrowsingHistory(Integer id ,Long userId){
        // 查看是否已存在该条浏览记录
        UserEssayBrowsingHistoryTbl historyTbl = Db.lambdaQuery(UserEssayBrowsingHistoryTbl.class)
                .eq(UserEssayBrowsingHistoryTbl::getUserId, userId)
                .eq(UserEssayBrowsingHistoryTbl::getEssayId, id)
                .one();
        if (historyTbl == null){
            // 不存在浏览记录，新增
            UserEssayBrowsingHistoryTbl userEssayBrowsingHistoryTbl = new UserEssayBrowsingHistoryTbl();
            userEssayBrowsingHistoryTbl.setUserId(userId.intValue());
            userEssayBrowsingHistoryTbl.setEssayId(id);
            userEssayBrowsingHistoryTbl.setWhenCreated(LocalDateTime.now());
            userEssayBrowsingHistoryTblService.save(userEssayBrowsingHistoryTbl);
        }else {
            historyTbl.setWhenCreated(LocalDateTime.now());
            userEssayBrowsingHistoryTblService.updateById(historyTbl); // 更新时间
        }

        // 查询是否超过50条记录
        Long count = Db.lambdaQuery(UserEssayBrowsingHistoryTbl.class)
                .eq(UserEssayBrowsingHistoryTbl::getUserId, userId.intValue())
                .count();

        if (count > 50){
            // 删除最旧的数据
            Db.lambdaUpdate(UserEssayBrowsingHistoryTbl.class)
                    .orderByAsc(UserEssayBrowsingHistoryTbl::getWhenCreated)
                    .last("limit 1")
                    .remove();
        }
    }
}
