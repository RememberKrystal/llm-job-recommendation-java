package com.remember.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.remember.domain.dto.GetPostDTO;
import com.remember.domain.po.PostTbl;
import com.remember.mapper.PostTblMapper;
import com.remember.result.Result;
import com.remember.service.IPostTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Service
public class PostTblServiceImpl extends ServiceImpl<PostTblMapper, PostTbl> implements IPostTblService {

    @Override
    public Result<List<PostTbl>> getAllPost(GetPostDTO getPostDTO) {
        // 构建分页条件
        Page<PostTbl> page = new Page<>(getPostDTO.getPage(), getPostDTO.getSize());
        Page<PostTbl> p = lambdaQuery()
                .like(getPostDTO.getName() != null, PostTbl::getPostName, getPostDTO.getName())
                .page(page);
        if (CollUtil.isEmpty(p.getRecords())){
            return Result.success(Collections.emptyList()); // 返回空集合
        }
        return Result.success(p.getRecords()); // 返回分页数据
    }
}
