package com.remember.service.impl;

import com.remember.domain.po.SpecializedTbl;
import com.remember.mapper.SpecializedTblMapper;
import com.remember.service.ISpecializedTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Service
public class SpecializedTblServiceImpl extends ServiceImpl<SpecializedTblMapper, SpecializedTbl> implements ISpecializedTblService {

}
