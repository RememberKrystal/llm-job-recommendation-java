package com.remember.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.po.PostSchoolTbl;
import com.remember.domain.po.PostTbl;
import com.remember.domain.po.UserPostBrowsingHistoryTbl;
import com.remember.mapper.UserPostBrowsingHistoryTblMapper;
import com.remember.service.IUserPostBrowsingHistoryTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户岗位浏览记录 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
public class UserPostBrowsingHistoryTblServiceImpl extends ServiceImpl<UserPostBrowsingHistoryTblMapper, UserPostBrowsingHistoryTbl> implements IUserPostBrowsingHistoryTblService {

    @Override
    public List<?> getUserPostBrowsingHistory(Integer school, int page, int size) {
        if (school == 1) {
            // 校招岗位
            List<UserPostBrowsingHistoryTbl> list = getPostList(1, page, size);
            if (list.isEmpty()){
                return new ArrayList<>();
            }
            List<Integer> PostSchoolIds = list.stream().map(UserPostBrowsingHistoryTbl::getPostId).toList();
            return Db.lambdaQuery(PostSchoolTbl.class).in(PostSchoolTbl::getId, PostSchoolIds).list();
        }
        // 普通岗位
        List<UserPostBrowsingHistoryTbl> list = getPostList(2, page, size);
        if (list.isEmpty()){
            return new ArrayList<>();
        }
        List<Integer> PostIds = list.stream().map(UserPostBrowsingHistoryTbl::getPostId).toList();
        return Db.lambdaQuery(PostTbl.class).in(PostTbl::getId, PostIds).list();
    }

    @Override
    public void removeAll(Integer school) {
        LambdaQueryWrapper<UserPostBrowsingHistoryTbl> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserPostBrowsingHistoryTbl::getUserId, BaseContext.getCurrentId());
        wrapper.eq(UserPostBrowsingHistoryTbl::getSchool, school); // 1是校招岗位，2不是校招岗位
        remove(wrapper);
    }

    private List<UserPostBrowsingHistoryTbl> getPostList(Integer school, int page, int size) {
        return lambdaQuery().eq(UserPostBrowsingHistoryTbl::getSchool, school)
                .eq(UserPostBrowsingHistoryTbl::getUserId, BaseContext.getCurrentId())
                .orderByDesc(UserPostBrowsingHistoryTbl::getWhenCreated) // 时间倒序
                .page(new Page<>(page, size))
                .getRecords();
    }
}
