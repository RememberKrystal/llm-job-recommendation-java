package com.remember.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.remember.domain.po.IndustryTrendsTbl;
import com.remember.mapper.IndustryTrendsTblMapper;
import com.remember.result.Result;
import com.remember.service.IIndustryTrendsTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Service
@RequiredArgsConstructor
public class IndustryTrendsTblServiceImpl extends ServiceImpl<IndustryTrendsTblMapper, IndustryTrendsTbl> implements IIndustryTrendsTblService {

    private final IndustryTrendsTblMapper industryTrendsTblMapper;

    private final StringRedisTemplate stringRedisTemplate;

    private static final String KEY = "cache:industryTrends:";
    @Override
    public Result<List<IndustryTrendsTbl>> getIndustryTrends(Integer industryId) {
        // 从缓存中获取行业趋势
        String key = KEY + industryId;
        String resultJson = stringRedisTemplate.opsForValue().get(key);
        if (resultJson != null) {
            // 存在返回数据
            List<IndustryTrendsTbl> list = JSONUtil.toList(resultJson, IndustryTrendsTbl.class);
            return Result.success(list);
        }
        // 不存在，查询数据库
        LambdaQueryWrapper<IndustryTrendsTbl> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(IndustryTrendsTbl::getIndustryId, industryId);
        List<IndustryTrendsTbl> list = industryTrendsTblMapper.selectList(wrapper);
        // 存入缓存
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(list));
        // 返回数据
        return Result.success(list);
    }
}
