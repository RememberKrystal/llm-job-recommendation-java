package com.remember.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.config.PostCacheConfig;
import com.remember.context.BaseContext;
import com.remember.controller.PostTblController;
import com.remember.domain.po.PostSchoolTbl;
import com.remember.domain.po.UserPostCollectionTbl;
import com.remember.mapper.PostSchoolTblMapper;
import com.remember.result.Result;
import com.remember.service.IPostSchoolTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.remember.utils.GetClientIpAddressUtil;
import com.remember.utils.UpdateUserViewsUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 秋招春招信息表
 * 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
@RequiredArgsConstructor
public class PostSchoolTblServiceImpl extends ServiceImpl<PostSchoolTblMapper, PostSchoolTbl> implements IPostSchoolTblService {

    private final StringRedisTemplate stringRedisTemplate;
    private final static String POST_SCHOOL_KEY = "cache:postSchool:";

    @Override
    public Result<List<PostSchoolTbl>> getAllPostSchool(int page, int size) {
        // 查询缓存
        String result = stringRedisTemplate.opsForValue().get(POST_SCHOOL_KEY);
        if (result != null) {
            // 存在直接返回
            List<PostSchoolTbl> list = JSONUtil.toList(result, PostSchoolTbl.class);
            // 对Redis中的数据进行分页
            int start = (page - 1) * size;
            int end = Math.min(page * size, list.size());
            if (start >= list.size()) {
                return Result.success(Collections.emptyList());  // 防止分页越界
            }
            list = list.subList(start, end);
            return Result.success(list);
        }
        // 不存在查询数据库
        List<PostSchoolTbl> list = this.list();
        // 缓存到Redis,并设置过期时间为24小时
        stringRedisTemplate.opsForValue().set(POST_SCHOOL_KEY, JSONUtil.toJsonStr(list), 60 * 60 * 24, TimeUnit.SECONDS);
        // 对Redis中的数据进行分页
        int start = (page - 1) * size;
        int end = Math.min(page * size, list.size());
        if (start >= list.size()) {
            return Result.success(Collections.emptyList());  // 防止分页越界
        }
        list = list.subList(start, end);
        return Result.success(list);
    }

    private final PostCacheConfig postCacheConfig; // 用于记录IP访问量

    private final ApplicationContext applicationContext; // spring上下文，用于获取代理对象

    @Override
    public Result<PostSchoolTbl> getPostSchoolGetById(Integer id, HttpServletRequest request) {
        // 1.根据id查询相关岗位
        PostSchoolTbl postSchoolTbl = getById(id);
        if (postSchoolTbl == null) {
            return Result.error("该校园岗位不存在");
        }
        // 2.存在，增加每日的浏览量
        // 2.1 获取id查看是否重复
        String ipAddress = GetClientIpAddressUtil.getClientIp(request);
        postCacheConfig.schoolIpCache().put(ipAddress + id, id); // 保证key值不重复（相同IP浏览不同岗位，Key不重复）

        // 3. 获取岗位的实际浏览量 = 缓存的浏览量 + 数据库的总浏览量
        Map<Integer, Integer> valueCount = new HashMap<>();
        for (String key : postCacheConfig.schoolIpCache().keySet()) {
            Integer value = postCacheConfig.schoolIpCache().get(key);
            valueCount.put(value, valueCount.getOrDefault(value, 0) + 1);
        }
        int size = valueCount.get(id);
        int views = getById(id).getViews();
        // 实际浏览量
        postSchoolTbl.setViews(size + views);

        // 4, 新增用户的浏览记录（这里采用异步执行）
        // 获取代理对象（事务）
        UpdateUserViewsUtil proxy = applicationContext.getBean(UpdateUserViewsUtil.class);
        proxy.updateUserViewAsync(id, BaseContext.getCurrentId(), 1);   // 调用异步方法,来处理用户浏览量操作

        // 5. 查看当前用户是否收藏该岗位
        Long collectionCount = Db.lambdaQuery(UserPostCollectionTbl.class)
                .eq(UserPostCollectionTbl::getUserId, BaseContext.getCurrentId().intValue())
                .eq(UserPostCollectionTbl::getPostId, id)
                .eq(UserPostCollectionTbl::getSchool, 1)
                .count();
        if (collectionCount > 0) {
            postSchoolTbl.setCollection(true);// 该用户已收藏该岗位
        }
        // 清空valueCount
        valueCount.clear();

        // 7. 返回数据
        return Result.success(postSchoolTbl);
    }

    @Override
    public Result<List<PostSchoolTbl>> getByYearOrSchool(Integer year, Integer school, int page, int size) {
        if (page <= 0 || size <= 0) {
            return Result.error("分页参数不合法");
        }

        // 构建分页查询
        Page<PostSchoolTbl> pageRequest = new Page<>(page, size);

        // 使用 MyBatis-Plus 的 lambdaQuery，动态拼接查询条件
        List<PostSchoolTbl> list = lambdaQuery()
                .eq(year != null, PostSchoolTbl::getYear, year)
                .eq(school != null, PostSchoolTbl::getSchool, school)
                .page(pageRequest)  // 执行分页查询
                .getRecords();

        // 处理没有数据的情况
        if (list.isEmpty()) {
            return Result.success(Collections.emptyList());  // 如果没有数据返回空列表
        }

        return Result.success(list);  // 返回查询结果
    }
}
