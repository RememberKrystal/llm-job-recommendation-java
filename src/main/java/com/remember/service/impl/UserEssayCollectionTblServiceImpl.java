package com.remember.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserEssayCollectionTbl;
import com.remember.domain.po.UserEssaySupportTbl;
import com.remember.mapper.UserEssayCollectionTblMapper;
import com.remember.result.Result;
import com.remember.service.IUserEssayCollectionTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户推文收藏表 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
public class UserEssayCollectionTblServiceImpl extends ServiceImpl<UserEssayCollectionTblMapper, UserEssayCollectionTbl> implements IUserEssayCollectionTblService {

    @Override
    public Result<List<EssayTbl>> getUserEssayCollection() {
        // 查询用户收藏的推文
        List<UserEssayCollectionTbl> list = lambdaQuery().eq(UserEssayCollectionTbl::getUserId, BaseContext.getCurrentId().intValue())
                .orderByDesc(UserEssayCollectionTbl::getWhenCreated)
                .list();

        if (list == null){
            return Result.success();
        }
        // 获取推文id集合
        List<Integer> idList = list.stream().map(UserEssayCollectionTbl::getEssayId).toList();
        // 查询推文信息
        List<EssayTbl> result = Db.lambdaQuery(EssayTbl.class)
                .in(EssayTbl::getId, idList)
                .list();
        for (EssayTbl essayTbl : result) {
            essayTbl.setCollection(true); // 设置为已收藏
            // 查看是否已点赞
            if (Db.lambdaQuery(UserEssaySupportTbl.class)
                    .eq(UserEssaySupportTbl::getUserId, BaseContext.getCurrentId().intValue())
                    .eq(UserEssaySupportTbl::getEssayId, essayTbl.getId())
                    .count() > 0){
                essayTbl.setSupport(true); // 设置为已点赞
            }
            // 转化为集合
            essayTbl.setImageList(JSONUtil.toList(essayTbl.getImage(), String.class));
        }
        return Result.success(result);
    }

    @Override
    public Result<?> collectionEssay(Integer id) {
        // 查询用户是否已经收藏了该推文
        UserEssayCollectionTbl collectionTbl = lambdaQuery().eq(UserEssayCollectionTbl::getUserId, BaseContext.getCurrentId().intValue())
                .eq(UserEssayCollectionTbl::getEssayId, id)
                .one();
        if (collectionTbl != null){
            // 已收藏，则取消收藏
            removeById(collectionTbl);
            // 更新推文收藏数量
            Db.lambdaUpdate(EssayTbl.class)
                    .eq(EssayTbl::getId, id)
                    .setSql("collection_number = collection_number - 1")
                    .update();
            return Result.success("取消收藏成功！");
        }
        // 未收藏
        // 新增一条收藏记录
        UserEssayCollectionTbl userEssayCollectionTbl = new UserEssayCollectionTbl();
        userEssayCollectionTbl.setUserId(BaseContext.getCurrentId().intValue());
        userEssayCollectionTbl.setEssayId(id);
        userEssayCollectionTbl.setWhenCreated(LocalDateTime.now());

        save(userEssayCollectionTbl);
        // 更新推文收藏数量
        Db.lambdaUpdate(EssayTbl.class)
                .eq(EssayTbl::getId, id)
                .setSql("collection_number = collection_number + 1")
                .update();

        return Result.success("收藏成功！");
    }
}
