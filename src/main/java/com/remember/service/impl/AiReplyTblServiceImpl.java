package com.remember.service.impl;

import com.remember.domain.po.AiReplyTbl;
import com.remember.mapper.AiReplyTblMapper;
import com.remember.service.IAiReplyTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Service
public class AiReplyTblServiceImpl extends ServiceImpl<AiReplyTblMapper, AiReplyTbl> implements IAiReplyTblService {

}
