package com.remember.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserEssayCollectionTbl;
import com.remember.domain.po.UserEssaySupportTbl;
import com.remember.mapper.UserEssaySupportTblMapper;
import com.remember.result.Result;
import com.remember.service.IEssayTblService;
import com.remember.service.IUserEssaySupportTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户推文点赞表 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
@RequiredArgsConstructor
public class UserEssaySupportTblServiceImpl extends ServiceImpl<UserEssaySupportTblMapper, UserEssaySupportTbl> implements IUserEssaySupportTblService {

    @Override
    public Result<List<EssayTbl>> getUserEssaySupport() {
        List<UserEssaySupportTbl> list = lambdaQuery()
                .eq(UserEssaySupportTbl::getUserId, BaseContext.getCurrentId().intValue())
                .orderByDesc(UserEssaySupportTbl::getWhenCreated)
                .list();

        if (list.isEmpty()){
            return Result.success();
        }

        List<Integer> idList = list.stream().map(UserEssaySupportTbl::getEssayId).toList();

        List<EssayTbl> resultList = Db.lambdaQuery(EssayTbl.class)
                .in(EssayTbl::getId, idList)
                .list();

        for (EssayTbl essayTbl : resultList) {
            essayTbl.setSupport(true); // 设置为已点赞

            // 查看是否收藏
            if (Db.lambdaQuery(UserEssayCollectionTbl.class)
                    .eq(UserEssayCollectionTbl::getUserId, BaseContext.getCurrentId().intValue())
                    .eq(UserEssayCollectionTbl::getEssayId, essayTbl.getId())
                    .count() > 0){
                essayTbl.setCollection(true);
            }
            // 转化为集合
            essayTbl.setImageList(JSONUtil.toList(essayTbl.getImage(), String.class));
        }

        return Result.success(resultList);
    }

    private final IEssayTblService essayTblService;

    @Override
    @Transactional
    public Result<?> supportEssay(Integer id) {
        EssayTbl essayTbl = Db.lambdaQuery(EssayTbl.class)
                .eq(EssayTbl::getId, id)
                .one();
        if (essayTbl == null){
            return Result.error("推文不存在");
        }

        UserEssaySupportTbl userEssaySupportTbl = lambdaQuery().eq(UserEssaySupportTbl::getUserId, BaseContext.getCurrentId().intValue())
                .eq(UserEssaySupportTbl::getEssayId, id)
                .one();

        if (userEssaySupportTbl != null){
            // 已经存在，则取消点赞
            removeById(userEssaySupportTbl);

            // 扣减点赞数量
            essayTbl.setSupportNumber(essayTbl.getSupportNumber() - 1);
            essayTblService.updateById(essayTbl);
            return Result.success("取消点赞成功！");
        }

        // 新增点赞记录
        UserEssaySupportTbl supportTbl = new UserEssaySupportTbl();
        supportTbl.setUserId(BaseContext.getCurrentId().intValue());
        supportTbl.setEssayId(id);
        supportTbl.setWhenCreated(LocalDateTime.now());
        save(supportTbl);

        essayTbl.setSupportNumber(essayTbl.getSupportNumber() + 1);
        essayTblService.updateById(essayTbl); // 更新点赞数量

        return Result.success("点赞成功！");

    }
}
