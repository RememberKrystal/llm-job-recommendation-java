package com.remember.service.impl;

import com.remember.domain.po.IndustryInfoTbl;
import com.remember.mapper.IndustryInfoTblMapper;
import com.remember.service.IIndustryInfoTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-08-21
 */
@Service
public class IndustryInfoTblServiceImpl extends ServiceImpl<IndustryInfoTblMapper, IndustryInfoTbl> implements IIndustryInfoTblService {

}
