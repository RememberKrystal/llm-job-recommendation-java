package com.remember.service.impl;

import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.dto.CollectionPostDTO;
import com.remember.domain.po.PostSchoolTbl;
import com.remember.domain.po.PostTbl;
import com.remember.domain.po.UserPostCollectionTbl;
import com.remember.mapper.UserPostCollectionTblMapper;
import com.remember.result.Result;
import com.remember.service.IUserPostCollectionTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 用户收藏岗位信息表 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
@RequiredArgsConstructor
public class UserPostCollectionTblServiceImpl extends ServiceImpl<UserPostCollectionTblMapper, UserPostCollectionTbl> implements IUserPostCollectionTblService {

    private final ApplicationContext applicationContext; // 用于获取代理对象

    @Override
    public Result<List<PostTbl>> getUserCollectionPost() {
        return getUserPostCollection();  // 获取普通岗位
    }

    @Override
    public Result<List<PostSchoolTbl>> getUserCollectionSchoolPost() {
        return getUserSchoolPostCollection();  // 获取校园岗位
    }


    @Override
    @Transactional
    public Result<?> collectionPost(CollectionPostDTO collectionPostDTO) {
        // 查询是否存在
        UserPostCollectionTbl userPostCollectionTbl = query().eq("user_id", BaseContext.getCurrentId())
                .eq("post_id", collectionPostDTO.getPostId())
                .eq("school", collectionPostDTO.getSchool())
                .one();
        if (userPostCollectionTbl != null) {
            // 已经收藏，则取消收藏
            removeById(userPostCollectionTbl);
            // 减少当前岗位的收藏数,开启异步执行
            UserPostCollectionTblServiceImpl bean = applicationContext.getBean(UserPostCollectionTblServiceImpl.class);
            bean.updatePostSubtractCollectionNumber(collectionPostDTO);

            return Result.success("取消收藏成功！");
        }
        // 未收藏，新增
        boolean save = save(new UserPostCollectionTbl(BaseContext.getCurrentId().intValue(),
                collectionPostDTO.getPostId(), collectionPostDTO.getSchool(), LocalDateTime.now()));
        // 增加当前岗位的收藏数，开启异步执行
        UserPostCollectionTblServiceImpl bean = applicationContext.getBean(UserPostCollectionTblServiceImpl.class);
        bean.updatePostAddCollectionNumber(collectionPostDTO);


        return save ? Result.success("收藏成功！") : Result.error("收藏岗位失败");
    }

    @Async // 开启异步线程
    public void updatePostSubtractCollectionNumber(CollectionPostDTO collectionPostDTO) {
        if (collectionPostDTO.getSchool() == 1) {
            // 校招岗位
            Db.lambdaUpdate(PostSchoolTbl.class)
                    .eq(PostSchoolTbl::getId, collectionPostDTO.getPostId())
                    .setSql("collection_number = collection_number - 1") // 减少收藏数量
                    .update();
        }
        // 普通岗位
        Db.lambdaUpdate(PostTbl.class)
                .eq(PostTbl::getId, collectionPostDTO.getPostId())
                .setSql("collection_number = collection_number - 1")
                .update();
    }

    @Async // 开启异步线程
    public void updatePostAddCollectionNumber(CollectionPostDTO collectionPostDTO) {
        if (collectionPostDTO.getSchool() == 1) {
            // 校招岗位
            Db.lambdaUpdate(PostSchoolTbl.class)
                    .eq(PostSchoolTbl::getId, collectionPostDTO.getPostId())
                    .setSql("collection_number = collection_number + 1") // 减少收藏数量
                    .update();
        }
        // 普通岗位
        Db.lambdaUpdate(PostTbl.class)
                .eq(PostTbl::getId, collectionPostDTO.getPostId())
                .setSql("collection_number = collection_number + 1")
                .update();
    }

    private Result<List<PostTbl>> getUserPostCollection() {
        List<UserPostCollectionTbl> list = query()
                .eq("user_id", BaseContext.getCurrentId().intValue())
                .eq("school", 2) // 2 非校招岗位
                .orderByDesc("when_created") // 按创建时间倒序
                .list();
        List<Integer> postIds = list.stream().map(UserPostCollectionTbl::getPostId).toList();

        if (postIds.isEmpty()) {
            return Result.success(new ArrayList<>());  // 返回空列表
        }

        List<PostTbl> postList = Db.lambdaQuery(PostTbl.class)
                .in(PostTbl::getId, postIds)
                .list();

        if (postList == null || postList.isEmpty()) {
            return Result.success(new ArrayList<>());  // 返回空列表
        }

        postList.forEach(item -> item.setCollection(true));  // 设置岗位已收藏
        return Result.success(postList);
    }

    private Result<List<PostSchoolTbl>> getUserSchoolPostCollection() {
        List<UserPostCollectionTbl> list = query()
                .eq("user_id", BaseContext.getCurrentId().intValue())
                .eq("school", 1) // 1 校招岗位
                .orderByDesc("when_created") // 按创建时间倒序
                .list();
        List<Integer> postIds = list.stream().map(UserPostCollectionTbl::getPostId).toList();

        if (postIds.isEmpty()) {
            return Result.success(new ArrayList<>());  // 返回空列表
        }

        List<PostSchoolTbl> postList = Db.lambdaQuery(PostSchoolTbl.class)
                .in(PostSchoolTbl::getId, postIds)
                .list();

        if (postList == null || postList.isEmpty()) {
            return Result.success(new ArrayList<>());  // 返回空列表
        }

        postList.forEach(item -> item.setCollection(true));  // 设置岗位已收藏
        return Result.success(postList);
    }


}
