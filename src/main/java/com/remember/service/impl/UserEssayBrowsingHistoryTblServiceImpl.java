package com.remember.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.toolkit.Db;
import com.remember.context.BaseContext;
import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserEssayBrowsingHistoryTbl;
import com.remember.mapper.UserEssayBrowsingHistoryTblMapper;
import com.remember.result.Result;
import com.remember.service.IUserEssayBrowsingHistoryTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户推文浏览记录表 服务实现类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
@Service
public class UserEssayBrowsingHistoryTblServiceImpl extends ServiceImpl<UserEssayBrowsingHistoryTblMapper, UserEssayBrowsingHistoryTbl> implements IUserEssayBrowsingHistoryTblService {

    @Override
    public Result<List<EssayTbl>> getUserEssayBrowsingHistory() {
        // 查询当前用户的推文浏览记录id集合
        List<UserEssayBrowsingHistoryTbl> list = lambdaQuery()
                .eq(UserEssayBrowsingHistoryTbl::getUserId, BaseContext.getCurrentId().intValue())
                .orderByDesc(UserEssayBrowsingHistoryTbl::getWhenCreated)
                .list();
        if (list.isEmpty()) {
            return Result.success();
        }
        List<Integer> idList = list.stream().map(UserEssayBrowsingHistoryTbl::getEssayId).toList();

        List<EssayTbl> resultList = Db.lambdaQuery(EssayTbl.class)
                .in(EssayTbl::getId, idList)
                .list();
        for (EssayTbl essayTbl : resultList) {
            essayTbl.setImageList(JSONUtil.toList(essayTbl.getImage(), String.class));
        }
        return Result.success(resultList);
    }

    @Override
    public Result<?> deleteUserEssayBrowsingHistory() {
        LambdaQueryWrapper<UserEssayBrowsingHistoryTbl> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserEssayBrowsingHistoryTbl::getUserId, BaseContext.getCurrentId().intValue());
        remove(wrapper);
        return Result.success("清理成功！");
    }
}
