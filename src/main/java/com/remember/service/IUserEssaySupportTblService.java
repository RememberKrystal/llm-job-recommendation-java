package com.remember.service;

import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserEssaySupportTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 * 用户推文点赞表 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IUserEssaySupportTblService extends IService<UserEssaySupportTbl> {

    Result<List<EssayTbl>> getUserEssaySupport();

    Result<?> supportEssay(Integer id);
}
