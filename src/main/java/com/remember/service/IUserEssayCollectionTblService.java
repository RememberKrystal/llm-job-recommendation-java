package com.remember.service;

import com.remember.domain.po.EssayTbl;
import com.remember.domain.po.UserEssayCollectionTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 * 用户推文收藏表 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IUserEssayCollectionTblService extends IService<UserEssayCollectionTbl> {

    Result<List<EssayTbl>> getUserEssayCollection();

    Result<?> collectionEssay(Integer id);
}
