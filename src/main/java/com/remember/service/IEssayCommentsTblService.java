package com.remember.service;

import com.remember.domain.dto.PublishEssayCommentsDTO;
import com.remember.domain.po.EssayCommentsTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.domain.vo.EssayCommentsVO;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 * 推文一级评论表 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IEssayCommentsTblService extends IService<EssayCommentsTbl> {

    Result<?> publishEssayComments(PublishEssayCommentsDTO publishEssayCommentsDTO);

    Result<List<EssayCommentsVO>> getEssayComments(Integer essayId);
}
