package com.remember.service;

import com.remember.domain.po.PostTbl;
import com.remember.domain.po.UserPostBrowsingHistoryTbl;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户岗位浏览记录 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IUserPostBrowsingHistoryTblService extends IService<UserPostBrowsingHistoryTbl> {

    List<?> getUserPostBrowsingHistory(Integer school,int page,int size);

    void removeAll(Integer school);

}
