package com.remember.service;

import com.remember.domain.dto.CollectionPostDTO;
import com.remember.domain.po.PostSchoolTbl;
import com.remember.domain.po.PostTbl;
import com.remember.domain.po.UserPostCollectionTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import java.util.List;

/**
 * <p>
 * 用户收藏岗位信息表 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IUserPostCollectionTblService extends IService<UserPostCollectionTbl> {

    Result<List<PostTbl>> getUserCollectionPost();

    Result<List<PostSchoolTbl>> getUserCollectionSchoolPost();

    Result<?> collectionPost(CollectionPostDTO collectionPostDTO);
}
