package com.remember.service;

import com.remember.domain.dto.PublishEssayDTO;
import com.remember.domain.po.EssayTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.result.Result;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 推文表
 服务类
 * </p>
 *
 * @author remember
 * @since 2024-09-21
 */
public interface IEssayTblService extends IService<EssayTbl> {

    Result<?> publishEssay(PublishEssayDTO publishEssayDTO);

    Result<?> updateEssay(EssayTbl essayTbl);

    Result<List<EssayTbl>> getAllEssay();

    Result<EssayTbl> getEssayById(Integer id, HttpServletRequest request);
}
