package com.remember.service;

import com.remember.domain.dto.UserAttachDTO;
import com.remember.domain.dto.UserBasicInfoDTO;
import com.remember.domain.dto.UserRegisterDTO;
import com.remember.domain.po.UserTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.remember.domain.vo.LoginSuccessVO;
import com.remember.result.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author remember
 * @since 2024-07-27
 */
public interface IUserTblService extends IService<UserTbl> {

    Result<String> registerUser(UserRegisterDTO userRegisterDTO);

    Result<String> sendSms(String phone);

    Result<LoginSuccessVO> login(String username, String password);

    Result<String> basicInfo(UserBasicInfoDTO basicInfoDTO);

    Result<?> updateUser(UserBasicInfoDTO userBasicInfoDTO);

    Result<?> updatePassword(String oldPassword, String newPassword);
}
