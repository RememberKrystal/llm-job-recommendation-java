package com.remember.service;

import com.remember.domain.po.PostTbl;
import com.remember.mapper.PostTblMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/*
 * @Author      : RememberKrystal
 * @Date        : 2024/12/3 10:05
 * @Description : 混合推荐算法实现（猜你喜欢）
 */
@Service
public class RecommendationService {

    @Autowired
    private PostTblMapper postTblMapper;

    /**
     * 混合推荐算法实现
     *
     * @param userId 用户ID
     * @param page   页码
     * @param size   每页大小
     * @return 推荐岗位列表
     */
    public List<PostTbl> recommendPosts(Long userId, Integer page, Integer size) {
        // 1. 获取推荐结果
        List<PostTbl> allRecommendations = generateRecommendations(userId);

        // 2. 分页处理
        int start = (page - 1) * size;
        int end = Math.min(start + size, allRecommendations.size());
        if (start >= allRecommendations.size()) {
            return Collections.emptyList(); // 页码超出范围时返回空列表
        }

        return allRecommendations.subList(start, end);
    }

    private List<PostTbl> generateRecommendations(Long userId) {
        // 调用混合推荐逻辑生成完整的推荐列表
        List<PostTbl> userFavoritePosts = getUserFavoritePosts(userId);
        List<PostTbl> userViewedPosts = getUserViewedPosts(userId);

        List<PostTbl> cfRecommendations = collaborativeFilteringRecommend(userFavoritePosts, userViewedPosts);
        List<PostTbl> contentRecommendations = contentBasedRecommend(userFavoritePosts, userViewedPosts);

        return mergeRecommendations(cfRecommendations, contentRecommendations);
    }

    private List<PostTbl> getUserFavoritePosts(Long userId) {
        // 根据用户ID查询收藏的普通岗位信息
        return postTblMapper.findUserFavorites(userId);
    }

    private List<PostTbl> getUserViewedPosts(Long userId) {
        // 根据用户ID查询浏览的普通岗位信息
        return postTblMapper.findUserViews(userId);
    }

    private List<PostTbl> collaborativeFilteringRecommend(List<PostTbl> favoritePosts, List<PostTbl> viewedPosts) {
        // 协同过滤逻辑：例如根据浏览和收藏的岗位的行业推荐相关岗位
        Set<Integer> industryIds = favoritePosts.stream()
                .map(PostTbl::getIndustryId)
                .collect(Collectors.toSet());
        industryIds.addAll(viewedPosts.stream()
                .map(PostTbl::getIndustryId)
                .collect(Collectors.toSet()));

        return postTblMapper.findPostsByIndustryIds(new ArrayList<>(industryIds));
    }

    private List<PostTbl> contentBasedRecommend(List<PostTbl> favoritePosts, List<PostTbl> viewedPosts) {
        // 基于内容推荐：例如匹配相同城市、经验、行业的岗位
        String city = favoritePosts.isEmpty() ? null : favoritePosts.get(0).getCity();
        String experience = favoritePosts.isEmpty() ? null : favoritePosts.get(0).getExperience();
        return postTblMapper.findPostsByCityAndExperience(city, experience);
    }

    private List<PostTbl> mergeRecommendations(List<PostTbl> cfRecommendations, List<PostTbl> contentRecommendations) {
        Map<Integer, PostTbl> recommendationMap = new HashMap<>();
        double cfWeight = 0.6;
        double contentWeight = 0.4;

        for (PostTbl post : cfRecommendations) {
            recommendationMap.put(post.getId(), post);
            post.setViews((int) (post.getViews() * cfWeight));
        }

        for (PostTbl post : contentRecommendations) {
            recommendationMap.merge(post.getId(), post, (existing, newPost) -> {
                existing.setViews(existing.getViews() + (int) (newPost.getViews() * contentWeight));
                return existing;
            });
        }

        // 根据权重排序返回
        return recommendationMap.values().stream()
                .sorted(Comparator.comparingInt(PostTbl::getViews).reversed())
                .collect(Collectors.toList());
    }
}

